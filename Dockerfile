# stage 1
FROM gradle:6.8.3-jdk11 AS gradleBuild

WORKDIR /home/gradle/project

COPY --chown=gradle:gradle . /home/gradle/project

RUN gradle build --no-daemon --stacktrace

# stage 2
FROM openjdk:11-jre

ENV spring_datasource_url=jdbc:postgresql://{GOBII_DB_SERVICE}:{GOBII_DB_TARGET_PORT}/{GOBII_DB_NAME}
ENV spring_datasource_username={GOBII_DB_USER}
ENV spring_datasource_password={GOBII_DB_PASSWORD}

COPY --from=gradleBuild /home/gradle/project/build/libs/ebs-sg-gdm-api.jar /app.jar

RUN mkdir -p /gobii_bundle/extractors/hdf5 \
    && mkdir -p /data/searchCalls

RUN curl https://bitbucket.org/ebsproject/gobii.hdf5/raw/HEAD/production/bin/fetchmarkerlist -o /gobii_bundle/extractors/hdf5/fetchmarkerlist \
    && chmod u+x /gobii_bundle/extractors/hdf5/fetchmarkerlist

ENTRYPOINT ["java", "-jar", "/app.jar"]
