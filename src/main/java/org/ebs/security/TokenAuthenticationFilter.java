package org.ebs.security;

import static org.ebs.Application.REQUEST_TOKEN;

import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import com.auth0.jwt.JWT;
import com.auth0.jwt.JWTVerifier;
import com.auth0.jwt.algorithms.Algorithm;
import com.auth0.jwt.exceptions.JWTVerificationException;
import com.auth0.jwt.interfaces.DecodedJWT;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Profile;
import org.springframework.http.HttpHeaders;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.stereotype.Component;

/**
 * Helper Filter for authentication. If a valid token is found in the request, a valid
 * Authentication is set in Spring Security Context during the life of this request.
 *
 * @author jarojas
 *
 */
@Component
@Profile({"default", "prod"})
class TokenAuthenticationFilter extends AbstractAuthenticationFilter {

    private static final Logger LOG = LoggerFactory.getLogger(TokenAuthenticationFilter.class);

    @Value("${ebs.sg.b4rapi.jwt-secret}")
    private String jwtSecret;

    @Autowired
    public TokenAuthenticationFilter(UserDetailsService userDetailsService) {
        super(userDetailsService);
        LOG.info("Creating authentication filter for PRODUCTION");
    }

    @Override
    public String getUsername(HttpServletRequest request) {
        String username = null;
        REQUEST_TOKEN.set(null);
        try {
            REQUEST_TOKEN.set(extractToken(request));
            DecodedJWT jwt = validateToken(REQUEST_TOKEN.get());
            username = Optional
                    .ofNullable(jwt.getClaim("http://wso2.org/claims/emailaddress").asString())
                    .orElseGet(() -> jwt.getClaim("email").asString());

            if (username == null) {
                throw new RuntimeException("Claim 'emailaddress' not found");
            }
        } catch (Exception e) {
            LOG.error("Error reading username from token: {}", e.getMessage());
            REQUEST_TOKEN.set(null);
        }

        return username;
    }

    private String extractToken(HttpServletRequest request) {
        return Optional.ofNullable(request.getHeader(HttpHeaders.AUTHORIZATION))
                .filter(header -> header.startsWith("Bearer ")).map(header -> header.substring(7))
                .orElseThrow(
                        () -> new IllegalArgumentException("Could not extract token from request"));
    }

    /**
     * @param token to verify
     * @return a verified and decoded JWT.
     * @throws IllegalArgumentException
     */
    private DecodedJWT validateToken(String token) {
        try {
            Algorithm algorithm = Algorithm.HMAC256(jwtSecret);
            JWTVerifier verifier = JWT.require(algorithm)
                .withIssuer("wso2")
                .build();
            return verifier.verify(token);
        } catch (JWTVerificationException exception){
            throw new IllegalArgumentException("Token is invalid");
        }
    }

}
