package org.ebs.rest;

import java.util.Optional;

import javax.validation.Valid;

import org.ebs.services.to.Callset;
import org.ebs.services.to.CallsetSearchRequest;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;


@Validated
@SecurityRequirement(name = "bearerAuth")
@ApiResponse(responseCode = "200", description = "OK")
@ApiResponse(responseCode = "400", description = "Bad request (usually an error in the request by the client)")
@ApiResponse(responseCode = "403", description = "Forbidden (missing or invalid authentication token)")
@ApiResponse(responseCode = "404", description = "Not Found (resource does not exist)")
@ApiResponse(responseCode = "500", description = "Server error (validation exceptions and internal problems)")
@Tag(name="CallSets")
public interface CallsetResource {
    @Operation(description = "Retrieves a single `CallSet` JSON object with the given `callSetDbId`.")
    BrResponse<Optional<Callset>> findCallset(
        @Parameter(description = "id of `CallSet` to be searched")
        @PathVariable
        Integer id
    );

    @Operation(description = "Gets an optionally filtered list of `CallSet` JSON objects.")
    BrPagedResponse<Callset> findCallsets(
        @RequestParam(required = false)
        @Parameter(description = "The ID of the `CallSet` to be retrieved.")
        String callSetDbId,
        @RequestParam(required = false)
        @Parameter(description = "The human readable name of the `CallSet` to be retrieved.")
        String callSetName,
        @RequestParam(required = false)
        @Parameter(description = "The ID of the `VariantSet` to be retrieved.")
        String variantSetDbId,
        @RequestParam(required = false)
        @Parameter(description = "The ID of the `Sample` to be retrieved.")
        String sampleDbId,
        @RequestParam(required = false)
        @Parameter(description = "Return only `CallSet`s generated from the `Sample` of this `Germplasm`.")
        String germplasmDbId,
        @RequestParam(required = false)
        @Parameter(description = "Number of records per page.")
        Integer pageSize,
        @RequestParam(required = false)
        @Parameter(description = "Results page you want to retrieve (0..N)")
        Integer page
    );

    @Operation(description =
        "Submit a search request for `CallSets`.<br>" +
        "Requires at least a partial `CallSearchRequest` in the POST body.<br>" +
        "Each provided search option is combined with boolean logic AND.<br>" +
        "Results are returned immediately."
    )
    BrPagedResponse<Callset> searchCallsets(
        @Valid
        CallsetSearchRequest params,
        @RequestParam(
            name = "pageSize",
            defaultValue = "1000"
        )
        Integer pageSize,
        @RequestParam(
            name = "page",
            defaultValue = "0"
        )
        Integer page
    );
}
