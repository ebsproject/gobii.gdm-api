package org.ebs.rest;

import java.util.Arrays;
import java.util.HashSet;
import org.ebs.services.CallService;
import org.ebs.services.to.Dimension;
import org.ebs.services.to.DimensionPage;
import org.ebs.services.to.SearchVariantMatrixParams;
import org.ebs.services.to.SearchVariantMatrixResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class VariantMatrixResourceImpl implements VariantMatrixResource {

    private final CallService callService;

    @Override
    @PostMapping("search/variantmatrix")
    public BrResponse<SearchVariantMatrixResponse> variantMatrix(@RequestBody
    SearchVariantMatrixParams params) {

        return BrapiResponseBuilder.forData(callService.findCalls(params)).build();
    }

    @Override
    @GetMapping("variantmatrix")
    public BrResponse<SearchVariantMatrixResponse> getVariantMatrix(
        Integer dimensionVariantPage,
        Integer dimensionVariantPageSize,
        Integer dimensionCallSetPage,
        Integer dimensionCallSetPageSize,
        String positionRange,
        String germplasmDbId,
        String germplasmName,
        String germplasmPUI,
        String callSetDbId,
        String variantDbId,
        String variantSetDbId,
        Boolean expandHomozygotes,
        String sepPhased,
        String sepUnphased,
        String unknownString
    ) {
        SearchVariantMatrixParams params = new SearchVariantMatrixParams();

        if (callSetDbId != null) params.setCallSetDbIds(Arrays.asList(callSetDbId));
        if (germplasmDbId != null) params.setGermplasmDbIds(new HashSet<String>(Arrays.asList(germplasmDbId)));
        if (germplasmName != null) params.setGermplasmNames(new HashSet<String>(Arrays.asList(germplasmName)));
        if (germplasmPUI != null) params.setGermplasmPUIs(new HashSet<String>(Arrays.asList(germplasmPUI)));
        if (variantDbId != null) params.setVariantDbIds(Arrays.asList(variantDbId));
        if (variantSetDbId != null) params.setVariantSetDbIds(new HashSet<String>(Arrays.asList(variantSetDbId)));

        // pagination
        DimensionPage variantPagination = new DimensionPage();
        variantPagination.setDimension(Dimension.variants);
        variantPagination.setPage(dimensionVariantPage);
        variantPagination.setPageSize(dimensionVariantPageSize);

        DimensionPage callsetPagination = new DimensionPage();
        callsetPagination.setDimension(Dimension.callsets);
        callsetPagination.setPage(dimensionCallSetPage);
        callsetPagination.setPageSize(dimensionCallSetPageSize);

        params.setPagination(Arrays.asList(variantPagination, callsetPagination));

        return BrapiResponseBuilder.forData(callService.findCalls(params)).build();
    }

}
