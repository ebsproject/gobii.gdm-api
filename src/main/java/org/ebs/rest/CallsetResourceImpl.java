package org.ebs.rest;

import java.util.Collections;
import java.util.Optional;

import org.ebs.services.CallsetService;
import org.ebs.services.to.Callset;
import org.ebs.services.to.CallsetSearchRequest;
import org.ebs.util.brapi.BrPagedResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;

import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;


@RestController
@RequestMapping(path="v2", produces="application/json")
@RequiredArgsConstructor
public class CallsetResourceImpl implements CallsetResource {

    private final CallsetService callsetService;

    @Override
    @GetMapping("callsets")
    public BrPagedResponse<Callset> findCallsets(
        String callSetDbId,
        String callSetName,
        String variantSetDbId,
        String sampleDbId,
        String germplasmDbId,
        Integer pageSize,
        Integer page
    ) {
        CallsetSearchRequest filters = new CallsetSearchRequest();
        if (callSetDbId != null) filters.setCallSetDbIds(Collections.singleton(Integer.valueOf(callSetDbId)));
        if (callSetName != null) filters.setCallSetNames(Collections.singleton(callSetName));
        if (variantSetDbId != null) filters.setVariantSetDbIds(Collections.singleton(variantSetDbId));
        if (sampleDbId != null) filters.setSampleDbIds(Collections.singleton(Integer.valueOf(sampleDbId)));
        if (germplasmDbId != null) filters.setGermplasmDbIds(Collections.singleton(Integer.valueOf(germplasmDbId)));

        page = page == null ? 0 : page;
        pageSize = pageSize == null ? 1000 : pageSize;
        Pageable pageable = PageRequest.of(page, pageSize);
        return BrapiResponseBuilder.forData(callsetService.searchCallsets(filters, pageable))
            .withStatusSuccess()
            .build();
    }

    @Override
    @GetMapping("callsets/{id}")
    public BrResponse<Optional<Callset>> findCallset(Integer id) {
        return BrapiResponseBuilder.forData(callsetService.searchCallsets(id))
            .withStatusSuccess()
            .build();
    }

    @Override
    @PostMapping("search/callsets")
    public BrPagedResponse<Callset> searchCallsets(
        @RequestBody(required = true)
        CallsetSearchRequest params,
        Integer pageSize,
        Integer page
    ) {
        Pageable pageable = PageRequest.of(page, pageSize);
        return BrapiResponseBuilder.forData(callsetService.searchCallsets(params, pageable))
            .withStatusSuccess()
            .build();
    }

}
