package org.ebs.rest;

import static org.springframework.http.HttpStatus.ACCEPTED;

import org.ebs.services.CallService;
import org.ebs.services.FileResolver;
import org.ebs.services.to.SearchCallsDbIdResponse;
import org.ebs.services.to.SearchCallsParams;
import org.ebs.services.to.SearchCallsResponse;
import org.ebs.util.brapi.BrResponse;
import org.ebs.util.brapi.BrapiResponseBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import lombok.RequiredArgsConstructor;

@RestController
@RequestMapping("v2")
@RequiredArgsConstructor
public class CallResourceImpl implements CallResource {

    private final CallService callService;
    private final FileResolver fileResolver;

    @Override
    @PostMapping("search/calls")
    @ResponseStatus(code = ACCEPTED)
    public BrResponse<SearchCallsDbIdResponse> searchCalls(@RequestBody
    SearchCallsParams params) {

        return BrapiResponseBuilder.forData(callService.findCalls(params)).build();
    }

    @Override
    @GetMapping("search/calls/{searchResultsDbId}")
    public BrResponse<SearchCallsResponse> searchCalls(@PathVariable
    String searchResultsDbId,
            @RequestParam(required = false, defaultValue = "0", name = "pageToken")
            int currentPageToken) {

        String prevPageToken = null, nextPageToken = null;

        int totalPages = fileResolver.totalPagesForSearch(searchResultsDbId);
        prevPageToken = currentPageToken - 1 < 0 ? null : String.valueOf(currentPageToken - 1);
        nextPageToken = currentPageToken + 1 == totalPages ? null
                : String.valueOf(currentPageToken + 1);

        return BrapiResponseBuilder
                .forData(callService.findCalls(searchResultsDbId, currentPageToken), prevPageToken,
                        "" + currentPageToken, nextPageToken)
                .withStatusSuccess().build();
    }
}
