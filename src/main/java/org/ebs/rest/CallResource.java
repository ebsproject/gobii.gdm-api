package org.ebs.rest;

import javax.validation.Valid;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

import org.ebs.services.to.SearchCallsDbIdResponse;
import org.ebs.services.to.SearchCallsParams;
import org.ebs.services.to.SearchCallsResponse;
import org.ebs.util.brapi.BrResponse;
import org.springframework.validation.annotation.Validated;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@Validated
@SecurityRequirement(name = "bearerAuth")
public interface CallResource {
    @Operation(description = "Submits a query to search for specific Calls", responses = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request (usually an error in the request by the client)"),
            @ApiResponse(responseCode = "403", description = "Forbidden (missing or invalid authentication token)"),
            @ApiResponse(responseCode = "404", description = "Not Found (resource does not exist)"),
            @ApiResponse(responseCode = "500", description = "Server error (validation exceptions and internal problems)") })
    @Tag(name = "Calls")
    BrResponse<SearchCallsDbIdResponse> searchCalls(@Valid
    @Parameter(description = "Search parameters. Right now only 'callSetDbIds' is required")
    SearchCallsParams params);

    @Operation(description = "Get the results of a calls search", responses = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request (usually an error in the request by the client)"),
            @ApiResponse(responseCode = "403", description = "Forbidden (missing or invalid authentication token)"),
            @ApiResponse(responseCode = "404", description = "Not Found (resource does not exist)"),
            @ApiResponse(responseCode = "500", description = "Server error (validation exceptions and internal problems)") })
    @Tag(name = "Calls")
    BrResponse<SearchCallsResponse> searchCalls(@NotNull
    @Parameter(description = "Search id returned by the server when submitting the search", required = true)
    String searchResultsDbId,
            @Parameter(description = "token to retrieve a specific results page", required = true)
            @Min(0)
            int pageToken);

}
