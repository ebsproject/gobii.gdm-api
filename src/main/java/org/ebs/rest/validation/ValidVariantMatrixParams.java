package org.ebs.rest.validation;

import static org.ebs.services.to.Dimension.callsets;
import static org.ebs.services.to.Dimension.variants;
import static org.ebs.services.to.DimensionPage.MAX_PAGE_SIZE;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import java.util.List;

import javax.validation.Constraint;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

import org.ebs.services.to.DimensionPage;
import org.ebs.services.to.SearchVariantMatrixParams;

@Documented
@Constraint(validatedBy = VariantMatrixParamsValidator.class)
@Target(ElementType.PARAMETER)
@Retention(RetentionPolicy.RUNTIME)
public @interface ValidVariantMatrixParams {
    String message()

    default "generic error message";

    Class<?>[] groups() default {};

    Class<? extends SearchVariantMatrixParams>[] payload() default {};

}

class VariantMatrixParamsValidator
        implements ConstraintValidator<ValidVariantMatrixParams, SearchVariantMatrixParams> {

    private static final DimensionPage DEFAULT_VARIANT_PAGE = new DimensionPage(variants, 0,
            MAX_PAGE_SIZE);;
    private static final DimensionPage DEFAULT_CALLSET_PAGE = new DimensionPage(callsets, 0,
            MAX_PAGE_SIZE);;

    @Override
    public boolean isValid(SearchVariantMatrixParams value, ConstraintValidatorContext context) {
        return hasValidPagination(value, context);
    }

    static final boolean hasValidPagination(SearchVariantMatrixParams value,
            ConstraintValidatorContext context) {

        if (value.getPagination() == null || value.getPagination().isEmpty()) {
            value.setPagination(List.of(DEFAULT_VARIANT_PAGE, DEFAULT_CALLSET_PAGE));
        } else {
            value.getPagination().stream().filter(p -> p.getDimension().equals(callsets))
                    .findFirst().ifPresentOrElse((p) -> {
                    }, () -> value.getPagination().add(DEFAULT_CALLSET_PAGE));
            value.getPagination().stream().filter(p -> p.getDimension().equals(variants))
                    .findFirst().ifPresentOrElse((p) -> {
                    }, () -> value.getPagination().add(DEFAULT_VARIANT_PAGE));
        }

        return true;
    }

}
