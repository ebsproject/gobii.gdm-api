package org.ebs.rest;

import javax.validation.Valid;

import org.ebs.rest.validation.ValidVariantMatrixParams;
import org.ebs.services.to.SearchVariantMatrixParams;
import org.ebs.services.to.SearchVariantMatrixResponse;
import org.ebs.util.brapi.BrResponse;

import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.RequestParam;

import io.swagger.v3.oas.annotations.Operation;
import io.swagger.v3.oas.annotations.Parameter;
import io.swagger.v3.oas.annotations.responses.ApiResponse;
import io.swagger.v3.oas.annotations.security.SecurityRequirement;
import io.swagger.v3.oas.annotations.tags.Tag;

@Validated
@SecurityRequirement(name = "bearerAuth")
@ApiResponse(responseCode = "200", description = "OK")
@ApiResponse(responseCode = "400", description = "Bad request (usually an error in the request by the client)")
@ApiResponse(responseCode = "403", description = "Forbidden (missing or invalid authentication token)")
@ApiResponse(responseCode = "404", description = "Not Found (resource does not exist)")
@ApiResponse(responseCode = "500", description = "Server error (validation exceptions and internal problems)")
@Tag(name = "VariantMatrix")
public interface VariantMatrixResource {

    @Operation(description = "Get the results of a calls search", responses = {
            @ApiResponse(responseCode = "200", description = "OK"),
            @ApiResponse(responseCode = "400", description = "Bad request (usually an error in the request by the client)"),
            @ApiResponse(responseCode = "403", description = "Forbidden (missing or invalid authentication token)"),
            @ApiResponse(responseCode = "404", description = "Not Found (resource does not exist)"),
            @ApiResponse(responseCode = "500", description = "Server error (validation exceptions and internal problems)") })
    @Tag(name = "VariantMatrix")
    BrResponse<SearchVariantMatrixResponse> variantMatrix(@Valid
    @ValidVariantMatrixParams
    @Parameter(description = "Search parameters")
    SearchVariantMatrixParams params);

    @Tag(name = "VariantMatrix")
    @Operation(description = "Gets a filtered list of two dimensional matrix representing the raw contents of a VCF as JSON.")
    BrResponse<SearchVariantMatrixResponse> getVariantMatrix(
        @RequestParam(required = false, defaultValue = "0")
        @Parameter(description = "The requested page number for the Variant dimension of the matrix.")
        Integer dimensionVariantPage,

        @RequestParam(required = false, defaultValue = "1000")
        @Parameter(description = "The requested page size for the Variant dimension of the matrix.")
        Integer dimensionVariantPageSize,

        @RequestParam(required = false, defaultValue = "0")
        @Parameter(description = "The requested page number for the CallSet dimension of the matrix.")
        Integer dimensionCallSetPage,

        @RequestParam(required = false, defaultValue = "1000")
        @Parameter(description = "The requested page size for the CallSet dimension of the matrix.")
        Integer dimensionCallSetPageSize,

        @RequestParam(required = false)
        @Parameter(description = "The postion range to search. Not supported yet.")
        String positionRange,

        @RequestParam(required = false)
        @Parameter(description = "Internal database identifier. Not supported yet.")
        String germplasmDbId,

        @RequestParam(required = false)
        @Parameter(description = "Name of the germplasm. Not supported yet.")
        String germplasmName,

        @RequestParam(required = false)
        @Parameter(description = "Permanent unique identifier (DOI, URI, etc.) Not supported yet.")
        String germplasmPUI,

        @RequestParam(required = false)
        @Parameter(description = "The ID of the CallSet to be retrieved.")
        String callSetDbId,

        @RequestParam(required = false)
        @Parameter(description = "The ID of the Variant to be retrieved.")
        String variantDbId,

        @RequestParam(required = false)
        @Parameter(description = "The ID of the VariantSet to be retrieved.")
        String variantSetDbId,

        @RequestParam(required = false, defaultValue = "true")
        @Parameter(description = "Should homozygotes be expanded (true) or collapsed into a single occurrence (false). Not supported yet.")
        Boolean expandHomozygotes,

        @RequestParam(required = false)
        @Parameter(description = "The string used as a separator for phased allele calls. Not supported yet.")
        String sepPhased,

        @RequestParam(required = false)
        @Parameter(description = "The string used as a separator for unphased allele calls. Not supported yet.")
        String sepUnphased,

        @RequestParam(required = false)
        @Parameter(description = "The string used as a representation for missing data. Not supported yet.")
        String unknownString
    );
}
