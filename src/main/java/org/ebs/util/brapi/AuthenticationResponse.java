package org.ebs.util.brapi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class AuthenticationResponse {

    private String access_token;
    private String refresh_token;
    private String scope;
    private String id_token;
    private String token_type;
    private int expires_in;
}