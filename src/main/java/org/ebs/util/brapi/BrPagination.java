package org.ebs.util.brapi;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents pagination section of a BrAPI response
 *
 * @author JAROJAS
 *
 */
@Getter
@Setter
@ToString
public abstract class BrPagination {

}
