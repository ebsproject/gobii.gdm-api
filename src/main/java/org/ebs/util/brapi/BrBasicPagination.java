package org.ebs.util.brapi;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Models traditional pagination, with total count, total pages, etc
 *
 * @author JAROJAS
 *
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class BrBasicPagination extends BrPagination {

    private int pageSize;
    private int currentPage;
    private long totalCount;
    private int totalPages;

}
