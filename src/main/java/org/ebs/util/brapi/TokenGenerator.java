package org.ebs.util.brapi;

import static java.lang.String.format;
import static java.net.http.HttpRequest.BodyPublishers.ofString;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.MINUTES;
import static java.time.temporal.ChronoUnit.SECONDS;
import static java.util.Base64.getEncoder;
import static java.util.Optional.ofNullable;
import static org.springframework.http.HttpHeaders.AUTHORIZATION;
import static org.springframework.http.HttpStatus.OK;

import java.io.IOException;
import java.net.URI;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;
import java.net.http.HttpResponse.BodyHandlers;
import java.time.Duration;
import java.time.Instant;
import java.util.Optional;

import com.fasterxml.jackson.databind.ObjectMapper;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@RequiredArgsConstructor
@Slf4j
public class TokenGenerator {

    @Value("${ebs.gobii.gdm.auth.client_id}")
    private String clientId;
    @Value("${ebs.gobii.gdm.auth.client_secret}")
    private String clientSecret;
    @Value("${ebs.gobii.gdm.auth.username}")
    private String userName;
    @Value("${ebs.gobii.gdm.auth.password}")
    private String password;
    @Value("${ebs.gobii.gdm.auth.endpoint}")
    private String authenticationEndpoint;

    private final ObjectMapper mapper;

    private HttpClient _client = HttpClient.newBuilder().build();
    private HttpRequest _req;

    private String token = null;
    private Instant expiration = now();

    public String getToken() {

        if (token == null || now().plus(2, MINUTES).isAfter(expiration)) {
            AuthenticationResponse auth = authenticate().orElseThrow(() -> {
                token = null;
                throw new RuntimeException("Could not generate token");
            });
            token = auth.getAccess_token();
            expiration = now().plus(auth.getExpires_in(), SECONDS);
            log.debug("Authentication generated. Valid from {} to {}", now(), expiration);

        }

        return token;
    }

    Optional<AuthenticationResponse> authenticate() {
        log.debug("Generating new authentication token...");
        AuthenticationResponse auth = null;
        try {
            HttpResponse<String> response = _client.send(authenticationRequest(),
                    BodyHandlers.ofString());

            if (response.statusCode() == OK.value()) {
                auth = mapper.readValue(response.body(), AuthenticationResponse.class);
            } else {
                log.warn("Authentication failed for {} with status: {}. Detail: {}", userName,
                        HttpStatus.valueOf(response.statusCode()), response.body());
            }

        } catch (IOException | InterruptedException e) {
            log.warn("Authentication failed for {}. Detail: {}", userName, e.getMessage());
        }
        return ofNullable(auth);
    }

    String authorizationHeader(String clientId, String clientSecret) {
        String credentials = format("%s:%s", clientId, clientSecret);
        return format("Basic %s", getEncoder().encodeToString(credentials.getBytes()));

    }

    String requestBody() {
        return format("grant_type=password&claims=username&scope=openid&username=%s&password=%s",
                userName, password);
    }

    HttpRequest authenticationRequest() {
        if (_req == null) {
            _req = HttpRequest.newBuilder(URI.create(authenticationEndpoint))
                    .timeout(Duration.ofSeconds(5))
                    .header(AUTHORIZATION, authorizationHeader(clientId, clientSecret))
                    .header(HttpHeaders.CONTENT_TYPE, "application/x-www-form-urlencoded")
                    .POST(ofString(requestBody())).build();
        }
        return _req;
    }
}