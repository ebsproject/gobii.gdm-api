package org.ebs.util;

import static org.ebs.util.Utils.copyNotNulls;

import java.lang.reflect.ParameterizedType;

import org.springframework.core.convert.converter.Converter;

public abstract class SimpleConverter<S, T> implements Converter<S, T> {

    @Override
    public T convert(S source) {
        T target = target();
        copyNotNulls(source, target);
        customize(source, target);
        return target;
    }

    /**
     * generates an instance of the target class at runtime
     *
     * @return an instance of T
     */
    @SuppressWarnings("unchecked")
    private final T target() {
        ParameterizedType superClass = (ParameterizedType) this.getClass().getGenericSuperclass();
        Class<T> type = (Class<T>) superClass.getActualTypeArguments()[1];
        try {
            return type.getDeclaredConstructor().newInstance();
        } catch (Exception e) {
            throw new RuntimeException("There is no default constructor for: " + type);
        }
    }

    /**
     * Empty method, called after initial conversion. Override this method to
     * perform further customizations on a target object
     *
     * @param source
     * @param target
     */
    protected void customize(S source, T target) {

    }

}
