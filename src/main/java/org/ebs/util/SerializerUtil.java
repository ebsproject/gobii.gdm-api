package org.ebs.util;

import java.util.ArrayList;
import java.util.List;

import com.esotericsoftware.kryo.Kryo;
import com.esotericsoftware.kryo.io.Input;
import com.esotericsoftware.kryo.io.Output;

import org.ebs.services.to.Call;

public class SerializerUtil {
    private static final ThreadLocal<Kryo> kryoLocal = ThreadLocal.withInitial(() -> {
        Kryo kryo = new Kryo();
        kryo.setRegistrationRequired(false);
        kryo.setCopyReferences(false);
        kryo.setWarnUnregisteredClasses(true);

        kryo.register(org.ebs.services.to.Call.class);
        kryo.register(java.util.ArrayList.class);
        kryo.register(org.ebs.services.to.CallQueryRecord.class);
        kryo.register(java.util.TreeMap.class);
        kryo.register(org.ebs.services.to.CallPageQuery.class);
        kryo.register(org.ebs.services.to.GenotypeCall.class);
        kryo.register(int[].class);

        return kryo;
    });

    public static byte[] encode(Object object) {
        Output output = new Output(102400, -1);
        kryoLocal.get().writeClassAndObject(output, object);
        output.flush();
        return output.toBytes();
    }

    @SuppressWarnings("unchecked")
    public static <T> T decode(byte[] bytes) {
        Input input = new Input(bytes);
        Object ob = kryoLocal.get().readClassAndObject(input);
        return (T) ob;
    }

    /**
     * Returns a subList from the passed Call list equivalent to a page of the given
     * page size and page number
     * 
     * @param callList
     *            original list
     * @param pageToken
     *            key of the page to retrieve.
     * @param pageSize
     *            (maximum) number of elements to return, can be less for last page.
     * @return
     */
    public static final List<Call> callSubList(List<Call> callList, int pageToken, int pageSize) {
        int fromIdx = pageToken * pageSize;
        int toIdx = Math.min(fromIdx + pageSize, callList.size());
        return new ArrayList<>(callList.subList(fromIdx, toIdx));
    }
}
