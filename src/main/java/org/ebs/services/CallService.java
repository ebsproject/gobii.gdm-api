package org.ebs.services;

import org.ebs.services.to.SearchCallsDbIdResponse;
import org.ebs.services.to.SearchCallsParams;
import org.ebs.services.to.SearchCallsResponse;
import org.ebs.services.to.SearchVariantMatrixParams;
import org.ebs.services.to.SearchVariantMatrixResponse;

public interface CallService {

    SearchCallsDbIdResponse findCalls(SearchCallsParams params);

    SearchCallsResponse findCalls(String searchResultsDbId, int pageToken);

    SearchVariantMatrixResponse findCalls(SearchVariantMatrixParams params);
}
