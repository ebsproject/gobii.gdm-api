package org.ebs.services;

import static java.nio.file.FileVisitOption.FOLLOW_LINKS;
import static java.nio.file.LinkOption.NOFOLLOW_LINKS;
import static java.time.Instant.now;
import static java.time.temporal.ChronoUnit.DAYS;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.time.Instant;
import java.util.stream.Stream;

import org.ebs.model.repository.DatasetRepository;
import org.ebs.util.SerializerUtil;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Component;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Component
@Slf4j
@RequiredArgsConstructor
public class FileResolver {

    @Value("${ebs.gobii.gdm.dataPath}")
    private String dataPath = "sample-data";
    private final String searchCallsPath = "searchCalls";
    @Value("${ebs.gobii.gdm.extractorsPath}")
    private String extractorsPath = "extractorsPath";

    private final DatasetRepository datasetRepository;

    OpenOption[] defaultOptions = new OpenOption[] {};

    public void persistForSearch(Object bean, String parentDir, String fileName)
            throws IOException {
        Path fullRelativePath = Paths.get(dataPath, searchCallsPath, parentDir);
        Files.createDirectories(fullRelativePath);

        Path filePath = Paths.get(fullRelativePath.toString(), fileName);
        Files.deleteIfExists(filePath);
        Files.createFile(filePath);
        Files.write(filePath, SerializerUtil.encode(bean), defaultOptions);
    }

    public Path resolveForSearch(String parentDir, String fileName) throws IOException {
        Files.createDirectories(Paths.get(dataPath, searchCallsPath, parentDir));
        return Paths.get(dataPath, searchCallsPath, parentDir, fileName);
    }

    public Path resolveForHDF5(String parentDir, int datasetId) {
        // for now ignore parentDir (crop)
        String fileName = String.format("DS_%s.h5", datasetId);
        return Paths.get(dataPath, "hdf5", fileName);
    }

    public Path resolveForExtractor(String extractor) {
        return Paths.get(extractorsPath, "hdf5", extractor);
    }

    public <T> T retrieveForSearch(String parentDir, String fileName) throws IOException {
        Path path = resolveForSearch(parentDir, fileName);
        return SerializerUtil.decode(Files.readAllBytes(path));
    }

    /**
     * Removes all temporary files used to generate results
     * 
     * @param searchResultsDbId
     *            name of the working directory for a given search request
     */
    public void cleanForSearch(String searchResultsDbId) throws IOException {
        cleanForSearch(searchResultsDbId, false);
    }

    /**
     * Removes temporary files from working directory
     * 
     * @param searchResultsDbId
     *            name of the working directory for a given search request
     * @param removeResults
     *            false only removes utility files, true removes also result files
     *            (full clean)
     * 
     */
    public void cleanForSearch(String searchResultsDbId, boolean removeResults) throws IOException {
        Path searchDir = Paths.get(dataPath, searchCallsPath, searchResultsDbId);

        Files.list(searchDir).filter(path -> {
            String fileName = path.getFileName().toString();
            return removeResults || fileName.equals("calls") || fileName.startsWith("position")
                    || fileName.startsWith("query");
        }).forEach(path -> {
            try {
                Files.deleteIfExists(path);
            } catch (IOException e) {
                log.error("Cannot delete: {}. Reason: {}", path, e.getMessage());
                e.printStackTrace();
            }
        });
    }

    public int totalPagesForSearch(String searchResultsDbId) {
        Path searchDir = Paths.get(dataPath, searchCallsPath, searchResultsDbId);

        int total = -1;
        try (Stream<Path> files = Files.find(searchDir, 1,
                (path, attribs) -> path.getFileName().toString().startsWith("calls."),
                FOLLOW_LINKS)) {

            total = (int) files.count();
        } catch (IOException e) {
            log.error("cannot read results: {}", e.getMessage());
            e.printStackTrace();
        }
        return total;
    }

    /**
     * Remove result files for searches older than the specified number of days
     * 
     * @param daysToRetain
     *            days a search must be stored before deletion
     */
    public void cleanUpSearchResults(int daysToRetain) {
        Path searchDir = Paths.get(dataPath, searchCallsPath);

        try {
            Files.list(searchDir).forEach(path -> {
                try {
                    Instant modifiedDate = Files.getLastModifiedTime(path, NOFOLLOW_LINKS)
                            .toInstant();
                    if (modifiedDate.plus(daysToRetain, DAYS).isBefore(now())) {
                        log.trace("REMOVING results for {}", path.getFileName());
                        cleanForSearch(path.getFileName().toString(), true);
                        Files.deleteIfExists(path);
                    }
                } catch (IOException e) {
                    log.error("Cannot delete: {}. Reason: {}", path, e.getMessage());
                    e.printStackTrace();
                }
            });
        } catch (IOException e) {
            log.error("Cannot read search folder: {}", e.getMessage());
            e.printStackTrace();
        }
    }

}
