package org.ebs.services;

import static java.util.stream.Collectors.toList;
import static java.util.stream.Collectors.toSet;
import static org.ebs.services.to.DimensionPage.MAX_PAGE_SIZE;

import java.io.IOException;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.HashSet;
import java.util.LinkedHashMap;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.TreeMap;
import java.util.UUID;
import java.util.stream.Collectors;

import org.ebs.model.CallsetModel;
import org.ebs.model.MarkerModel;
import org.ebs.model.repository.CallsetRepository;
import org.ebs.model.repository.MarkerRepository;
import org.ebs.services.to.Call;
import org.ebs.services.to.CallPageQuery;
import org.ebs.services.to.CallQueryRecord;
import org.ebs.services.to.Callset;
import org.ebs.services.to.CallsetSearchRequest;
import org.ebs.services.to.Dimension;
import org.ebs.services.to.DimensionPage;
import org.ebs.services.to.DimensionPageResponse;
import org.ebs.services.to.SearchCallsDbIdResponse;
import org.ebs.services.to.SearchCallsParams;
import org.ebs.services.to.SearchCallsResponse;
import org.ebs.services.to.SearchVariantMatrixParams;
import org.ebs.services.to.SearchVariantMatrixResponse;
import org.ebs.services.to.Variant;
import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.util.Pair;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@RequiredArgsConstructor
@Service
@Transactional(readOnly = true)
@Slf4j
public class CallServiceImpl implements CallService {

    private final CallsetRepository callsetRepository;
    private final MarkerRepository markerRepository;
    private final ConversionService converter;
    private final HDF5Service hdf5Service;
    private final FileResolver fileResolver;
    private final CallsetService callsetService;

    @Override
    public SearchCallsDbIdResponse findCalls(SearchCallsParams params) {

        List<CallsetModel> callsets = callsetRepository.findByCallSetDbIdIn(
                params.getCallSetDbIds().stream()
                .mapToInt(id -> Integer.parseInt(id)).toArray());

        Map<String, List<MarkerModel>> markersByDataset = extractMarkersByDataset(callsets, null);

        if (log.isTraceEnabled()) {
            markersByDataset.forEach((k, v) -> {
                log.trace("DatasetId: {}, Markers: {}", k, v.size());
            });
        }

        String searchResultsDbId = generateCallSubQueries(callsets, markersByDataset,
                params.getPageSize());

        hdf5Service.extractGenotypes(searchResultsDbId);

        return new SearchCallsDbIdResponse(searchResultsDbId);

    }

    /**
     * generates a map where the key is the datasetId, and the value a markerModel
     * list associated to such dataset
     * 
     * @param callsets
     *            to heve their associated markers extracted
     * @param datasetFilter
     *            filters which datasets to include, ignored if it's null or empty
     * @return
     */
    Map<String, List<MarkerModel>> extractMarkersByDataset(List<CallsetModel> callsets,
            Collection<String> datasetFilter) {
        Map<String, List<MarkerModel>> markersByDataset = new LinkedHashMap<>();
        callsets.forEach(callset -> {

            callset.getVariantSetDbIds().stream()
                .filter(v -> datasetFilter == null || datasetFilter.isEmpty() || datasetFilter.contains(v))
                .forEach(variantSetId -> {
                    markersByDataset.putIfAbsent(variantSetId,
                            markerRepository.findByDatasetId(Integer.parseInt(variantSetId)));
                });
        });
        return markersByDataset;
    }

    /**
     * Generates the pre-computed pages to request genotype data, allowing to only
     * use the database once. Returns the calls of the first page, too.
     *
     * @return
     */
    String generateCallSubQueries(List<CallsetModel> callsets,
            Map<String, List<MarkerModel>> markersByDataset, int pageSize) {

        // A map where the key is the pageToken, the value is a list of CallQueryPage
        // objects needed to complete a single page in the general query. This can
        // happen when a single page returns records from more than one dataset.
        Map<String, List<CallPageQuery>> queryPages = new TreeMap<>();

        List<CallQueryRecord> queryRecords = new ArrayList<>();
        List<Call> calls = new ArrayList<>();
        // calls in a callset = sum of markers-by-dataset associated to that callset
        callsets.forEach(callset -> {
            callset.getDatasetDnaRunIdx().fieldNames().forEachRemaining(datasetId -> {
                markersByDataset.get(datasetId).forEach(marker -> {

                    Call c = converter.convert(Arrays.asList(callset, datasetId, marker),
                            Call.class);
                    calls.add(c);

                    queryRecords.add(new CallQueryRecord(Integer.parseInt(datasetId),
                            callset.getDatasetDnaRunIdx().get(datasetId).asInt(),
                            marker.getDatasetMarkerIdx().get(datasetId).asInt(), null));
                });
            });
        });

        if (log.isTraceEnabled()) {
            for (CallQueryRecord qr : queryRecords) {
                log.trace("QueryRecord: {}", qr);
            }
        }

        int token = 0, count = pageSize; // so it generates page 1 in first iteration
        Set<Integer> samples = null;
        Set<Integer> markers = null;
        List<CallPageQuery> subPages = null;
        CallPageQuery subPage = null;
        int nextDatasetId = -1;

        for (int i = 0; i < queryRecords.size(); i++) {
            CallQueryRecord current = queryRecords.get(i);

            if (i + 1 < queryRecords.size())
                nextDatasetId = queryRecords.get(i + 1).getDatasetId();

            if (count % pageSize == 0) { // start a new page
                count = 0;
                subPages = new ArrayList<>();
                samples = new LinkedHashSet<>();
                markers = new LinkedHashSet<>();
                subPage = new CallPageQuery();

                subPage.setDatasetId(queryRecords.get(i).getDatasetId());
                subPages.add(subPage);

                queryPages.put(String.valueOf(token++), subPages);
            }
            count++;

            samples.add(current.getSample());
            markers.add(current.getMarker());

            if (nextDatasetId != current.getDatasetId() || i + 1 == queryRecords.size()
                    || count == pageSize) {
                subPage.setMarker(markers);
                subPage.setSample(samples);

                if (nextDatasetId != current.getDatasetId()) {
                    subPage = new CallPageQuery();
                    markers = new LinkedHashSet<>();
                    samples = new LinkedHashSet<>();
                    subPage.setDatasetId(nextDatasetId);
                    subPages.add(subPage);
                }
            }

        }

        if (log.isTraceEnabled()) {
            queryPages.forEach((k, v) -> {
                log.trace("Final Page {}: {}", k, v);
            });
        }

        try {
            return persistQueryData(queryRecords, queryPages, calls, pageSize);
        } catch (Exception e) {
            e.printStackTrace();
            throw new RuntimeException("Could not generate search job: " + e.getMessage());
        }

    }

    /**
     *
     * @param queryRecords
     *            matrix of samples vs markers
     * @param queryPages
     *            to assemble genotype result pages
     * @param calls
     *            resulting data
     * @param pageSize
     * @return the searchDbId
     * @throws IOException
     */

    String persistQueryData(List<CallQueryRecord> queryRecords,
            Map<String, List<CallPageQuery>> queryPages, List<Call> calls, int pageSize)
            throws IOException {

        String searchCallsDbId = UUID.randomUUID().toString() + "." + pageSize;

        fileResolver.persistForSearch(queryRecords, searchCallsDbId, "queryRecords");
        fileResolver.persistForSearch(queryPages, searchCallsDbId, "queryPages");
        fileResolver.persistForSearch(calls, searchCallsDbId, "calls");

        return searchCallsDbId;

    }

    @Override
    public SearchCallsResponse findCalls(String searchResultsDbId, int pageToken) {

        List<Call> calls = null;
        try {
            log.trace("deserializing calls...");
            calls = fileResolver.retrieveForSearch(searchResultsDbId, "calls." + pageToken);
        } catch (Exception e) {
            throw new RuntimeException("Cannot read result Calls: " + e.getMessage());
        }

        SearchCallsResponse response = new SearchCallsResponse();
        response.setData(calls);
        response.setSepPhased(HDF5Service.UNPHASED_SEP);
        return response;
    }

    /**
     * Looks for calls with the provided filters
     * 
     * @return the BrAPI search/variantmatrix response object. The matrix is in
     *         'data'
     */
    @Override
    public SearchVariantMatrixResponse findCalls(SearchVariantMatrixParams params) {

        SearchVariantMatrixResponse response = new SearchVariantMatrixResponse();

        List<Pair<Integer, CallsetModel>> callsets = getfilteredCallsets(params);

        response.setCallSetDbIds(callsets.stream()
                .map(c -> c.getSecond().getCallSetDbId().toString())
                .collect(toList()));

        List<CallsetModel> plainCallsets = callsets.stream()
                .map(c -> c.getSecond())
                .collect(toList());
        Map<String, List<MarkerModel>> markersByDataset = extractMarkersByDataset(plainCallsets,
                params.getVariantSetDbIds());
        response.setVariantSetDbIds(new ArrayList<>(markersByDataset.keySet()));

        // filter variants by variantsets and ids
        Map<MarkerModel, Set<Integer>> filteredVariants = new LinkedHashMap<>();
        markersByDataset.forEach((ds, mrks) -> {

            if (params.getVariantSetDbIds().isEmpty() || params.getVariantSetDbIds().contains(ds)) {
                mrks.stream()
                        .filter(variant -> params.getVariantDbIds().isEmpty() || params
                                .getVariantDbIds().contains(variant.getMarkerId().toString()))
                        .forEach(m -> {
                            filteredVariants.putIfAbsent(m, new HashSet<>());
                            Set<Integer> datasetsForMarker = filteredVariants.get(m);
                            m.getDatasetMarkerIdx().fieldNames().forEachRemaining(
                                    d -> datasetsForMarker.add(Integer.valueOf(d)));
                        });
            }
        });

        response.setVariants(
            filteredVariants.keySet().stream()
                .map(m -> converter.convert(m, Variant.class))
                .collect(Collectors.toCollection(() -> new LinkedHashSet<>())));

        response.setPagination(getPagination(params, plainCallsets, response.getVariants()));

        if (!params.isPreview()) {
            response.setData(retrieveData(response, params, callsets, filteredVariants));
        }

        return response;
    }

    List<Pair<Integer, CallsetModel>> getfilteredCallsets(SearchVariantMatrixParams params) {

        CallsetSearchRequest callsetSearchReq = converter.convert(params,
                CallsetSearchRequest.class);

        int pageNum = 0;
        Pageable page = PageRequest.of(pageNum++, MAX_PAGE_SIZE);
        List<Integer> callsetIds = new ArrayList<>();

        Page<Callset> pageCallset = collectCallsetDbIdsFromPage(callsetSearchReq, page, callsetIds);

        while (pageCallset.getNumberOfElements() > 0) {
            page = PageRequest.of(pageNum++, DimensionPage.MAX_PAGE_SIZE);
            pageCallset = collectCallsetDbIdsFromPage(callsetSearchReq, page, callsetIds);
        }

        List<CallsetModel> filteredCallsets = callsetRepository
                .findByCallSetDbIdIn(callsetIds.stream().mapToInt(i -> i.intValue()).toArray());

        return filteredCallsets.stream()
                .map(c -> Pair.of(Integer.valueOf(c.getDatasetIds().get(0)), c))
                .collect(toList());
    }

    /**
     * Collects callsetDbIds into the provided List
     * 
     * @param req
     *            parameters of the search
     * @param page
     *            to look for
     * @param callsetIds
     *            collector of the ids found in the given page of the result
     * @return the original Page<Callset> generated
     */
    Page<Callset> collectCallsetDbIdsFromPage(CallsetSearchRequest req, Pageable page,
            List<Integer> callsetIds) {
        Page<Callset> pageCallset = callsetService.searchCallsets(req, page);
        callsetIds.addAll(
            pageCallset.getContent().stream()
                .map(c -> c.getCallSetDbId())
                .collect(toSet()));
        return pageCallset;
    }

    List<DimensionPageResponse> getPagination(SearchVariantMatrixParams params,
            List<CallsetModel> callsets, Set<Variant> variants) {

        DimensionPage pageCallsets = params.getPagination().stream()
                .filter(p -> p.getDimension().equals(Dimension.callsets))
                .findFirst().get();
        DimensionPage pageVariants = params.getPagination().stream()
                .filter(p -> p.getDimension().equals(Dimension.variants))
                .findFirst().get();

        DimensionPageResponse pageCallsetsResp = new DimensionPageResponse(pageCallsets);
        DimensionPageResponse pageVariantsResp = new DimensionPageResponse(pageVariants);

        pageCallsetsResp.setTotalCount(callsets.size());
        pageCallsetsResp.setTotalPages((int) Math
                .ceil(pageCallsetsResp.getTotalCount() / (float) pageCallsetsResp.getPageSize()));

        int totalVariants = variants.size();
        pageVariantsResp.setTotalCount(totalVariants);
        pageVariantsResp.setTotalPages((int) Math
                .ceil(pageVariantsResp.getTotalCount() / (float) pageVariantsResp.getPageSize()));

        if (pageVariantsResp.getTotalPages() <= pageVariantsResp.getPage()) {
            throw new RuntimeException("Invalid page number for Variant Dimension, maximum is: "
                    + (pageVariantsResp.getTotalPages() - 1));
        }
        if (pageCallsetsResp.getTotalPages() <= pageCallsetsResp.getPage()) {
            throw new RuntimeException("Invalid page number for Callset Dimension, maximum is: "
                    + (pageCallsetsResp.getTotalPages() - 1));
        }

        return List.of(pageVariantsResp, pageCallsetsResp);
    }

    String[][] retrieveData(SearchVariantMatrixResponse response, SearchVariantMatrixParams params,
            List<Pair<Integer, CallsetModel>> callsets,
            Map<MarkerModel, Set<Integer>> filteredVariants) {

        // element 0 is variant page - from getPagination()
        int page = response.getPagination().get(0).getPage();
        int size = response.getPagination().get(0).getPageSize();

        // to use subList method of List
        List<Pair<MarkerModel, Set<Integer>>> variants = new ArrayList<>();
        filteredVariants.forEach((m, datasets) -> {
            variants.add(Pair.of(m, datasets));
        });

        List<Pair<MarkerModel, Set<Integer>>> variantPage = variants.subList(page * size,
                Math.min(page * size + size, variants.size()));

        // element 1 is callset page - from getPagination()
        page = response.getPagination().get(1).getPage();
        size = response.getPagination().get(1).getPageSize();

        List<Pair<Integer, CallsetModel>> callsetPage = callsets.subList(page * size,
                Math.min(page * size + size, callsets.size()));

        Map<Integer, CallPageQuery> subPagesByVariantSet = new HashMap<>();

        log.trace("callsetPage: {}, variantPage: {}", callsetPage, variantPage);
        boolean noMatches = true;

        for (Pair<Integer, CallsetModel> callset : callsetPage) {
            Integer variantSetId = callset.getFirst();
            CallPageQuery subPage = new CallPageQuery();
            for (Pair<MarkerModel, Set<Integer>> variant : variantPage) {

                // only matching (variant x callset) for a given dataset are considered
                if (variant.getSecond().contains(variantSetId)) {
                    noMatches = false;
                    subPagesByVariantSet.putIfAbsent(variantSetId, new CallPageQuery(variantSetId));
                    subPage = subPagesByVariantSet.get(variantSetId);

                    Set<Integer> subPageMarkers = subPage.getMarker();
                    Set<Integer> subPageVariantDbIds = subPage.getVariantDbIds();
                    // We add the POSITIONS (not the ids) of variants
                    subPageMarkers.add(variant.getFirst().getDatasetMarkerIdx()
                            .get("" + variantSetId).asInt());
                    subPageVariantDbIds.add(variant.getFirst().getMarkerId());

                }
            }
            Set<Integer> subPageCallsets = subPage.getSample();
            Set<Integer> subPageCallsetDbIds = subPage.getCallsetDbIds();

            // We add the POSITIONS (not the ids) of callsets
            subPageCallsets
                    .add(callset.getSecond().getDatasetDnaRunIdx().get("" + variantSetId).asInt());
            subPageCallsetDbIds.add(callset.getSecond().getCallSetDbId());
        }
        // no callsets matching any variant
        if (noMatches) {
            return new String[variantPage.size()][callsetPage.size()];
        }

        Map<Integer, List<String[]>> variantSubMatrices = extractSubMatricesData(
                subPagesByVariantSet);

        log.trace("subPagesByVariantSet: {}", subPagesByVariantSet);

        validateResultsCount(subPagesByVariantSet, variantSubMatrices);
        return assembleFullMatrix(variantPage, callsetPage, variantSubMatrices,
                subPagesByVariantSet);

    }

    /**
     * Extracts genotype data using hdf5Service for all sub-matrices needed in a page
     * 
     * @param subPagesByVariantSet
     *            pages defining the matrices to be extracted
     * @return a Map where the key is a variantsetId and the value the subMatrix for it
     */
    Map<Integer, List<String[]>> extractSubMatricesData(
            Map<Integer, CallPageQuery> subPagesByVariantSet) {
        Map<Integer, List<String[]>> variantSubMatrices = new HashMap<>();
        for (Integer variantSetId : subPagesByVariantSet.keySet()) {

            UUID tmpId = UUID.randomUUID();
            CallPageQuery pageQuery = subPagesByVariantSet.get(variantSetId);

            Path positionFilePath = hdf5Service.generatePositionFileForPage(tmpId.toString(),
                    pageQuery);

            log.trace("generated position file at {}", positionFilePath);

            List<String[]> variantSubMatrix = null;
            try {
                Path genotypesPath = hdf5Service.runExtractionFromDataFile(positionFilePath,
                        pageQuery);
                variantSubMatrix = hdf5Service.readVariantMatrixFromResultFile(genotypesPath);
                variantSubMatrices.put(variantSetId, variantSubMatrix);
            } catch (InterruptedException | IOException e) {
                log.error("could not generate matrix . Message: {}", e.getMessage());
                e.printStackTrace();
            }
        }

        return variantSubMatrices;

    }

    /**
     * generates a matrix of the given size and fills it with the values from
     * sub-matrices
     * 
     * @param variantSize
     *            number of variants in the matrix
     * @param callsetSize
     *            number of callsets in the matrix
     * @param variantSubMatrices
     *            map with call values by variantsetId
     * @param subPagesByVariantSet
     *            map with sub-regions from different variantsets that conform a page
     * @return a matrix of the given size with data embedded from variant
     *         sub-matrices
     */
    String[][] assembleFullMatrix(List<Pair<MarkerModel, Set<Integer>>> variantPage,
            List<Pair<Integer, CallsetModel>> callsetPage,
            Map<Integer, List<String[]>> variantSubMatrices,
            Map<Integer, CallPageQuery> subPagesByVariantSet) {
        String[][] variantMatrix = new String[variantPage.size()][callsetPage.size()];

        List<Integer> variantIds = variantPage.stream()
                .map(vp -> vp.getFirst().getMarkerId())
                .collect(toList());
        List<Integer> callsetIds = callsetPage.stream()
                .map(cp -> cp.getSecond().getCallSetDbId())
                .collect(toList());

        for (Integer variantSetId : variantSubMatrices.keySet()) {
            List<String[]> variantSubMatrix = variantSubMatrices.get(variantSetId);
            CallPageQuery subPageMeta = subPagesByVariantSet.get(variantSetId);

            int v = -1, c = -1;
            for (Integer mappingVariantId : subPageMeta.getVariantDbIds()) {
                c = -1;
                v++;
                for (Integer mappingCallsetId : subPageMeta.getCallsetDbIds()) {
                    int i = variantIds.indexOf(mappingVariantId);
                    int j = callsetIds.indexOf(mappingCallsetId);
                    c++;
                    String value = variantSubMatrix.get(v)[c];
                    variantMatrix[i][j] = value;
                }
            }
        }

        return variantMatrix;

    }

    /**
     * Checks if the number of elements returned by extractors for each subPage
     * matches the expected size
     * 
     * @throws RuntimeException
     *             if a dimension's size for a region in the matrix where values are
     *             expected (submatrix) does not match the expected size
     */
    void validateResultsCount(Map<Integer, CallPageQuery> subPagesByVariantSet,
            Map<Integer, List<String[]>> variantSubMatrices) {
        int expected = 0, actual = 0;

        for (Integer variantSetId : subPagesByVariantSet.keySet()) {
            expected = subPagesByVariantSet.get(variantSetId).getMarker().size();
            actual = variantSubMatrices.get(variantSetId).size();
            if (expected != actual) {
                throw new RuntimeException(String.format(
                        "Inconsistent number of variants for variantSet %s. Expected: %s. Returned: %s",
                        variantSetId, expected, actual));
            }

            expected = subPagesByVariantSet.get(variantSetId).getSample().size();
            actual = variantSubMatrices.get(variantSetId).get(0).length;
            if (actual != expected) {
                throw new RuntimeException(String.format(
                        "Inconsistent number of callsets for variantSet %s. Expected: %s. Returned: %s",
                        variantSetId, expected, actual));

            }
        }

    }

}
