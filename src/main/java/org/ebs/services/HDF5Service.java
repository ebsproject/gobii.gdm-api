package org.ebs.services;

import java.io.IOException;
import java.nio.file.Path;
import java.util.List;

import org.ebs.services.to.Call;
import org.ebs.services.to.CallPageQuery;

public interface HDF5Service {
    public static final String UNPHASED_SEP = "/";
    public static final String PHASED_SEP = "|";
    public static final String UNKNOWN_STRING = "N";

    /**
     * Extracts genotypes and load them into appropriate entities for a specific
     * search
     *
     * @param searchResultsDbId
     *            the search query to have genotype information set
     */
    void extractGenotypes(String searchResultsDbId);

    /**
     * for a given page in search results (Calls), it tries to generate result files
     * to set calls in such page genotype values
     * 
     * @param searchResultsDbId
     *            id of the search query
     * @param pageToken
     *            page requested
     * @param subpages
     *            subpages to make a page, size can be greter than 1
     * @param callsPage
     *            calls of the given page having their genotypes set
     * @throws IOException
     */
    void generateResultsForPage(String searchResultsDbId, int pageToken,
            List<CallPageQuery> subpages, List<Call> callsPage) throws IOException;

    /**
     * Executes a read from one HF5 file and store the results in a .genotype file
     * (a.k.a Results file)
     * 
     * @param positionFile
     *            file path for the markers file
     * @param subpage
     *            atomic sub set of elements to search for
     * @return the path of the generated Results file. It matches positionFile's
     *         name with the suffix .genotype
     * @throws InterruptedException
     * @throws IOException
     */
    Path runExtractionFromDataFile(Path positionFile, CallPageQuery subpage)
            throws InterruptedException, IOException;

    /**
     * Generates the default position file (position.list) in the corresponding
     * directory with the positions of markers defined in subpage.marker
     * 
     * @param searchResultsDbId
     *            parent directory, associated to a search
     * @param subpage
     *            contains the marker positions to put in the file
     */
    Path generatePositionFileForPage(String searchResultsDbId, CallPageQuery subpage);

    /**
     * Generates a position file in the corresponding directory with the positions
     * of markers defined in subpage.marker
     * 
     * @param searchResultsDbId
     *            parent directory, associated to a search
     * @param subpage
     *            contains the marker positions to put in the file
     * @param positionFileName
     *            name of the file to generate
     */
    Path generatePositionFileForPage(String searchResultsDbId, CallPageQuery subpage,
            String positionFileName);

    /**
     * Reads a .genotype file, formats it's values and returns the matrix without
     * further processing.
     * 
     * @param genotypeFile
     *            file to format
     * @return a 2 dimension matrix where first dimension represents variants and
     *         second one callsets
     */
    List<String[]> readVariantMatrixFromResultFile(Path genotypeFile);

}
