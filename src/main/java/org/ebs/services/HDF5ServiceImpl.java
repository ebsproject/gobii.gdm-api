package org.ebs.services;

import static java.nio.file.Files.readAllBytes;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.OpenOption;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.concurrent.CompletableFuture;

import org.apache.commons.lang3.StringUtils;
import org.ebs.services.to.Call;
import org.ebs.services.to.CallPageQuery;
import org.ebs.services.to.GenotypeCall;
import org.ebs.util.SerializerUtil;
import org.ebs.util.Utils;
import org.springframework.stereotype.Service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;

@Service
@Slf4j
@RequiredArgsConstructor
public class HDF5ServiceImpl implements HDF5Service {

    private final FileResolver fileResolver;

    @Override
    public void extractGenotypes(String searchResultsDbId) {
        CompletableFuture<Void> job = CompletableFuture.runAsync(() -> {
            processGenotypeExtraction(searchResultsDbId);
        });

        job.getNow(null);
    }

    void processGenotypeExtraction(String searchResultsDbId) {

        try {
            List<Call> callsRecords = fileResolver.retrieveForSearch(searchResultsDbId, "calls");
            log.trace("total calls read in POST: {}", callsRecords.size());
            int pageSize = Integer.parseInt(searchResultsDbId.split("\\.")[1]);

            log.trace("Reading query pages...");
            Map<String, List<CallPageQuery>> queryPages = SerializerUtil.decode(
                    readAllBytes(fileResolver.resolveForSearch(searchResultsDbId, "queryPages")));

            for (Entry<String, List<CallPageQuery>> entry : queryPages.entrySet()) {
                List<CallPageQuery> pageQuery = entry.getValue();
                int pageToken = Integer.parseInt(entry.getKey());

                log.trace("processing token {}, callsRecords size {}", pageToken,
                        callsRecords.size());

                List<Call> callsPage = SerializerUtil.callSubList(callsRecords, pageToken,
                        pageSize);

                log.trace("sub list size: {}", callsPage.size());
                generateResultsForPage(searchResultsDbId, pageToken, pageQuery, callsPage);
                log.trace("callsPage size after generateResultsForPage: {}", callsPage.size());
            }

            // after all genotypes set in calls, persist them once
            int persistedCount = 0;
            int pageNum = 0;
            while (persistedCount < callsRecords.size()) {
                List<Call> page = SerializerUtil.callSubList(callsRecords, pageNum, pageSize);
                fileResolver.persistForSearch(page, searchResultsDbId, "calls." + pageNum++);
                persistedCount += pageSize;
            }

            fileResolver.cleanForSearch(searchResultsDbId);

        } catch (IOException e) {

            e.printStackTrace();
        }
    }

    /**
     * for a given page in search results (Calls), it tries to generate result files
     * to set calls in such page genotype values
     *
     * @param searchResultsDbId
     *            id of the search query
     * @param pageToken
     *            page requested
     * @param subpages
     *            subpages to make a page, size can be greter than 1
     * @param callsPage
     *            calls of the given page having their genotypes set
     * @throws IOException
     */
    @Override
    public void generateResultsForPage(String searchResultsDbId, int pageToken,
            List<CallPageQuery> subpages, List<Call> callsPage) throws IOException {
        int i = 0;
        for (CallPageQuery subpage : subpages) {

            String positionFileName = String.format("position-%s.%s.list", pageToken, i++);
            Path positionFile = generatePositionFileForPage(searchResultsDbId, subpage,
                    positionFileName);

            try {
                Path genotypesPath = runExtractionFromDataFile(positionFile, subpage);
                log.trace("generated: " + genotypesPath);

                readFromResultFile(genotypesPath, callsPage);

            } catch (InterruptedException | IOException e) {
                log.error("cannot extract genotypes: " + e.getMessage());

            }
        }
    }

    @Override
    public Path generatePositionFileForPage(String searchResultsDbId, CallPageQuery subpage) {
        return generatePositionFileForPage(searchResultsDbId, subpage, "position.list");
    }

    @Override
    public Path generatePositionFileForPage(String searchResultsDbId, CallPageQuery subpage,
            String positionFileName) {

        Path positionFile = null;
        try {
            positionFile = Files
                    .createFile(fileResolver.resolveForSearch(searchResultsDbId, positionFileName));
            log.trace("writing position file at {} for {}", positionFile, subpage);
            Files.write(positionFile, Utils.join(subpage.getMarker(), "\n").getBytes(),
                    new OpenOption[] {});
        } catch (IOException e) {
            e.printStackTrace();
        }
        return positionFile;
    }

    /**
     * Executes a read from one HF5 file and store the results in a .genotype file
     * (a.k.a Results file)
     *
     * @param positionFile
     *            file path for the markers file
     * @param subpage
     *            atomic sub set of elements to search for
     * @return the path of the generated Results file. It matches positionFile's
     *         name with the suffix .genotype
     * @throws InterruptedException
     * @throws IOException
     */
    @Override
    public Path runExtractionFromDataFile(Path positionFile, CallPageQuery subpage)
            throws InterruptedException, IOException {

        // name of the file where results are put
        Path genotypeFile = Paths.get(positionFile.toString() + ".genotype");
        // receives success status and other output from process
        File outputFile = new File(positionFile.toString() + ".out");
        // receives errors from process
        File errorFile = new File(positionFile.toString() + ".err");

        String crop = "maize"; // TODO resolve crop by request
        Path hdf5Path = fileResolver.resolveForHDF5(crop, subpage.getDatasetId());
        Path extractorPath = fileResolver.resolveForExtractor("fetchmarkerlist");

        log.trace("executing... {} markers-fast {} {} {}", extractorPath, hdf5Path.toString(),
                positionFile.toString(), genotypeFile.toString());

        new ProcessBuilder(
                extractorPath.toString(),
                "markers-fast",
                hdf5Path.toString(),
                positionFile.toString(),
                genotypeFile.toString())
            .redirectError(errorFile)
            .redirectOutput(outputFile)
            .start().waitFor();

        filterBySamples(genotypeFile, subpage.getSample());

        return genotypeFile;
    }

    void filterBySamples(final Path genotypeFile, final Set<Integer> samples)
            throws InterruptedException, IOException {
        Path tmpFile = Paths.get(genotypeFile + ".tmp");
        Files.move(genotypeFile, tmpFile, StandardCopyOption.REPLACE_EXISTING);

        String cutSamples = samplesForCut(samples);

        log.trace("executing... cut -f {} {}", cutSamples, tmpFile.toString());

        new ProcessBuilder("cut", "-f", cutSamples, tmpFile.toString())
                .redirectOutput(new File(genotypeFile.toString()))
                .start().waitFor();

        Files.deleteIfExists(tmpFile);
    }

    String samplesForCut(final Set<Integer> samples) {
        List<Integer> sampleList = new ArrayList<>(samples.size());

        Iterator<Integer> samplesIter = samples.iterator();
        while (samplesIter.hasNext()) {
            Integer sample = samplesIter.next();
            if (sample != -1) {
                sampleList.add(sample + 1);
            }
        }

        return StringUtils.join(sampleList, ",");
    }

    /**
     * Reads genotypes in a Results File and fill in the appropriate Call objects.
     * IMPORTANT: order in .genotypes file must honor order of markers passed,
     * otherwise markers must be ordered before genopype extraction
     *
     * @param genotypesPath
     * @param callsPage
     * @throws IOException
     */
    void readFromResultFile(Path genotypesPath, List<Call> callsPage) throws IOException {

        Set<Call> processedCalls = new HashSet<>();
        BufferedReader br = null;
        FileReader fr = null;

        fr = new FileReader(new File(genotypesPath.toString()));
        br = new BufferedReader(fr);

        // TODO add offset for when a previous page already read some dnarun's markers,
        // so not all markers in current page need to be read again

        List<String[]> markersByRun = new ArrayList<>();

        br.lines().forEach(line -> {
            markersByRun.add(line.split("\\t"));
        });

        int m = 0;
        int s = 0;

        for (Call call : callsPage) {
            String genotype = formatCall(markersByRun.get(m)[s]);

            m = m + 1 == markersByRun.size() ? 0 : m + 1;
            s = m == 0 ? s + 1 : s;

            GenotypeCall g = new GenotypeCall(Arrays.asList(genotype));
            call.setGenotype(g);
            processedCalls.add(call);
        }

        log.trace("callsPage: {}, removing: {}", callsPage.size(), processedCalls.size());
        callsPage.removeAll(processedCalls);
        log.trace("new callsPage: {}", callsPage.size());
        br.close();
        fr.close();
    }

    @Override
    public List<String[]> readVariantMatrixFromResultFile(Path genotypesPath) {
        List<String[]> matrix = new ArrayList<>();
        BufferedReader br = null;
        FileReader fr = null;
        try {
            fr = new FileReader(new File(genotypesPath.toString()));
            br = new BufferedReader(fr);

            br.lines().forEach(line -> {
                String[] callValues = line.split("\\t");
                for (int i = 0; i < callValues.length; i++) {
                    callValues[i] = formatCall(callValues[i]);
                }
                matrix.add(callValues);

            });

            br.close();
            fr.close();
        } catch (IOException e) {

        }
        return matrix;
    }

    String formatCall(String callValue) {

        return callValue.contains(UNKNOWN_STRING) ? UNKNOWN_STRING
                : String.join(UNPHASED_SEP, callValue.split(""));
    }

}
