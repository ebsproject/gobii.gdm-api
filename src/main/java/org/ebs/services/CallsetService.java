package org.ebs.services;

import java.util.Optional;

import org.ebs.services.to.Callset;
import org.ebs.services.to.CallsetSearchRequest;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;


public interface CallsetService {

    /**
     * Used to search for single IDs.
     * @param params
     * @return Optional<Callset>
     */
    public Optional<Callset> searchCallsets( Integer id );

    /**
     * Used to find all, by using an empty CallsetSearchRequest
     * @param page
     * @return Page<Callset>
     */
    public Page<Callset> searchCallsets( Pageable page );

    /**
     * Used to do a search with parameters and pages.
     * @param params
     * @param page
     * @return Page<Callset>
     */
    public Page<Callset> searchCallsets( CallsetSearchRequest params, Pageable page );

}
