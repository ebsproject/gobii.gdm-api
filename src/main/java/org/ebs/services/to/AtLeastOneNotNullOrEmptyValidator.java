package org.ebs.services.to;

import java.lang.reflect.Field;
import java.util.Set;
import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class AtLeastOneNotNullOrEmptyValidator implements ConstraintValidator<AtLeastOneNotNullOrEmpty, CallsetSearchRequest> {

    @SuppressWarnings("unchecked")
    @Override
    public boolean isValid(CallsetSearchRequest object, ConstraintValidatorContext context) {
        Field[] fields = object.getClass().getDeclaredFields();
        for (Field field : fields) {
            try {
                field.setAccessible(true);
                Object value = field.get(object);
                if (
                    (value instanceof Set<?> && !((Set<Object>) value).isEmpty())
                    || (value != null && !(value instanceof Set<?>))
                ) {
                    return true;
                }
            }
            catch (Exception e) {
                return false;
            }
        }
        return false;
    }

}
