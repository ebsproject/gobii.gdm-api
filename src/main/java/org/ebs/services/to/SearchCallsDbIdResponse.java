package org.ebs.services.to;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@AllArgsConstructor
public class SearchCallsDbIdResponse {
    private String searchResultsDbId;
}
