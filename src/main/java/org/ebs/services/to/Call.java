package org.ebs.services.to;

import java.io.Serializable;

import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@EqualsAndHashCode(doNotUseGetters = true, onlyExplicitlyIncluded = true)
public class Call implements Serializable {

    private static final long serialVersionUID = 654987L;

    private String additionalInfo;
    @EqualsAndHashCode.Include
    private Integer callSetDbId;
    private String callSetName;
    private GenotypeCall genotype;
    // private List<Integer> genotype_likelihood;
    private String phaseSet;
    @EqualsAndHashCode.Include
    private String variantDbId;
    private String variantName;

}
