package org.ebs.services.to;

import javax.validation.constraints.Min;

import org.hibernate.validator.constraints.Range;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
@AllArgsConstructor
public class DimensionPage {

    public static final int MAX_PAGE_SIZE = 1000;

    @Parameter(description = "The dimension of the matrix being paginated")
    Dimension dimension;
    @Parameter(description = "The requested page number (zero indexed)")
    @Min(0)
    int page;
    @Parameter(description = "The maximum number of elements per page in this dimension of the matrix")
    @Range(min = 100, max = MAX_PAGE_SIZE)
    int pageSize;

}
