package org.ebs.services.to;

import java.io.Serializable;
import java.util.List;
import java.util.Map;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
public class Callset implements Serializable {

    private Integer callSetDbId;
    private String callSetName;
    private Integer studyDbId;
    private Integer sampleDbId;
    private String sampleName;
    private Integer germplasmDbId;
    private String germplasmName;
    private String germplasmType;
    private List<String> variantSetDbIds;
    private Map<String, String> additionalInfo;

}
