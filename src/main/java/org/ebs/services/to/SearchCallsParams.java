package org.ebs.services.to;

import static org.ebs.services.to.DimensionPage.MAX_PAGE_SIZE;

import java.util.List;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchCallsParams {
    @NotEmpty(message = "at least one callSetDbId is required")
    @Size(max = MAX_PAGE_SIZE)
    @Parameter(required = true, description = "List of callset DB identifiers, as returned by /search/callsets")
    private List<String> callSetDbIds;
    private boolean expandHomozygotes;
    @Min(1)
    @Max(DimensionPage.MAX_PAGE_SIZE)
    @Parameter(required = true, description = "Token of the results page requested, starts with 0")
    private int pageSize = DimensionPage.MAX_PAGE_SIZE;
    private String pageToken;
    private String sepPhased;
    private String sepUnphased;
    private String unknownString;
    private List<String> variantDbIds;
    private List<String> variantSetDbIds;
}
