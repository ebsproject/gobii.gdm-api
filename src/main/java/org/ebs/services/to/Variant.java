package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class Variant {
    String contig;
    int end;
    int ploidy;
    int start;
    String variantDbId;

}
