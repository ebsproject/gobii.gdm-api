package org.ebs.services.to;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.validation.Valid;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class SearchVariantMatrixParams extends SearchVariantMatrixBase {

    @Parameter(description = "The germplasm to search. By DB id")
    Set<String> germplasmDbIds = new HashSet<>();
    @Parameter(description = "The germplasm to search. By name")
    Set<String> germplasmNames = new HashSet<>();
    @Parameter(description = "The germplasm to search. By permanent unique identifiers (PUI)")
    Set<String> germplasmPUIs = new HashSet<>();
    @Parameter(description = "Pagination for the matrix")
    List<@Valid DimensionPage> pagination;
    @Parameter(description = "The postion range to search. Not supported yet but left here for future work")
    List<String> positionRanges;
    @Parameter(description = "If set to true, the server should only return the lists of 'callSetDbIds', 'variantDbIds', and 'variantSetDbIds'. The server should not return any matrix data. This is intended to be a preview and give the client a sense of how large the matrix returned will be. Not supported yet, defaults to false")
    boolean preview;
    @Parameter(description = "The samples to search. By DB id")
    List<String> sampleDbIds = new ArrayList<>();

    @Parameter(description = "The Variant to search")
    List<String> variantDbIds = new ArrayList<>();
    @Parameter(description = "The VariantSet to search")
    Set<String> variantSetDbIds = new HashSet<>();

}
