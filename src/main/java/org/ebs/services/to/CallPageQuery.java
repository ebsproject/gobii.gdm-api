package org.ebs.services.to;

import java.io.Serializable;
import java.util.LinkedHashSet;
import java.util.Set;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents the sub matrix of market vs sample vs dataset to be returned in a
 * single page during a call query request. It is a pre-computed list of
 * arguments needed to retrieve calls for a specific page in a query. A single
 * page may require more than one instance of this class
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CallPageQuery implements Serializable {

    private int datasetId;
    /**
     * ids of samples in dnaRuns
     */
    private Set<Integer> sample = new LinkedHashSet<>();
    /**
     * positions of markers in a dataset, from marker.datasetMarkerIdx->value,
     * a.k.a. variants
     */
    private Set<Integer> marker = new LinkedHashSet<>();

    /**
     * db ids for marker set, both must match size and order
     */
    private Set<Integer> variantDbIds = new LinkedHashSet<>();
    /**
     * db ids for sample set, both must match size and order
     */
    private Set<Integer> callsetDbIds = new LinkedHashSet<>();

    public CallPageQuery(int datasetId) {
        this.datasetId = datasetId;
    }
}
