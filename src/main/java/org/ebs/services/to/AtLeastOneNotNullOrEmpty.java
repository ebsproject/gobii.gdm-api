package org.ebs.services.to;

import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;
import javax.validation.Constraint;
import javax.validation.Payload;


@Documented
@Constraint(validatedBy = AtLeastOneNotNullOrEmptyValidator.class)
@Target({ ElementType.TYPE })
@Retention(RetentionPolicy.RUNTIME)
public @interface AtLeastOneNotNullOrEmpty {
    String message() default "Empty search query or invalid arguments";
    Class<?>[] groups() default {};
    Class<? extends Payload>[] payload() default {};
}
