# Transfer Object Layer

```java
@Getter @Setter @ToString
public class Experiment {

    //simple validations go here (@NotNull, @NotBlank, @Min, @Pattern, etc)
    //json manipulation goes here (@JsonIgnore, @JsonProperty, etc)
}
```
