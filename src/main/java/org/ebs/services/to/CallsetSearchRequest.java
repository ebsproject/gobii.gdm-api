package org.ebs.services.to;

import java.util.HashSet;
import java.util.Set;

import javax.validation.constraints.Size;

import lombok.Getter;
import lombok.Setter;


@Getter
@Setter
@AtLeastOneNotNullOrEmpty
public class CallsetSearchRequest {
    @Size(max = 1000, message = "Only 1000 callSetIds allowed per query")
    private Set<Integer> callSetDbIds = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 callSetNames allowed per query")
    private Set<String> callSetNames = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 sampleDbIds allowed per query")
    private Set<Integer> sampleDbIds = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 sampleNames allowed per query")
    private Set<String> sampleNames = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 samplePUIs allowed per query")
    private Set<String> samplePUIs = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 variantSetDbIds allowed per query")
    private Set<String> variantSetDbIds = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 germplasmPUIs allowed per query")
    private Set<String> germplasmPUIs = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 germplasmDbIds allowed per query")
    private Set<Integer> germplasmDbIds = new HashSet<>();

    @Size(max = 1000, message = "Only 1000 germplasmNames allowed per query")
    private Set<String> germplasmNames = new HashSet<>();
}
