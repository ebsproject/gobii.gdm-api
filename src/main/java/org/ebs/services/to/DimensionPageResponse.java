package org.ebs.services.to;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
@NoArgsConstructor
public class DimensionPageResponse extends DimensionPage {

    @Parameter(description = "The total number of elements that are available on the server and match the requested query parameters")
    int totalCount;
    @Parameter(description = "The total number of pages of elements available on the server. This should be calculated with the following formula: totalPages = CEILING( totalCount / requested_page_size)")
    int totalPages;

    public DimensionPageResponse(DimensionPage dimensionPage) {
        if (dimensionPage != null) {
            this.setPage(dimensionPage.getPage());
            this.setPageSize(dimensionPage.getPageSize());
            this.setDimension(dimensionPage.getDimension());
        }
    }
}
