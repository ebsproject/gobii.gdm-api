package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;


@Getter @Setter
public class CallsetSearchResult {
    String searchResultDbId;
}
