package org.ebs.services.to;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

/**
 * Complimentary information about the variant matrix data. as an additional
 * overlay
 */
@Getter
@Setter
@ToString
public class GenotypeField {
    String fieldAbbreviation;

    @ToString.Exclude
    String[][] fieldMatrix;

    String fieldName;

    String fieldType;
}
