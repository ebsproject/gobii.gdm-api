package org.ebs.services.to;

import java.io.Serializable;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import lombok.ToString;

/**
 * Represents a single record in the result of a call query, that is the
 * specific marker and sample to look for in a given dataset.
 */
@Getter
@Setter
@AllArgsConstructor
@NoArgsConstructor
@ToString
public class CallQueryRecord implements Serializable {
    private int datasetId;
    private int sample;
    private int marker;
    private String genotype;
}
