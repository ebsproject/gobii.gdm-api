package org.ebs.services.to;

import java.util.List;

import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.Size;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public abstract class SearchVariantMatrixBase {
    @NotEmpty(message = "at least one callSetDbId is required")
    @Size(max = DimensionPage.MAX_PAGE_SIZE)
    @Parameter(required = true, description = "List of the CallSet to search, as returned by /search/callsets")
    List<String> callSetDbIds;

    @Parameter(description = "Should homozygotes be expanded (true) or collapsed into a single occurrence (false). Not supported yet, defaults to 'true'")
    boolean expandHomozygotes;

    @Parameter(description = "The string used as a separator for phased allele calls")
    String sepPhased = "|";

    @Parameter(description = "The string used as a separator for unphased allele calls")
    String sepUnphased = "/";

    @Parameter(description = "The string used as a representation for missing data")
    String unknownString = ".";

}
