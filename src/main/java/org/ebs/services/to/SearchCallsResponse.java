package org.ebs.services.to;

import java.util.List;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class SearchCallsResponse {
    private List<Call> data;
    private boolean expandHomozygotes;
    private String sepPhased;
    private String sepUnphased;
    private String unknownString;
}
