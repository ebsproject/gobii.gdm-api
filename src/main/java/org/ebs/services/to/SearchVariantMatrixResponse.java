package org.ebs.services.to;

import static org.ebs.services.HDF5Service.PHASED_SEP;
import static org.ebs.services.HDF5Service.UNKNOWN_STRING;
import static org.ebs.services.HDF5Service.UNPHASED_SEP;

import java.util.List;
import java.util.Set;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Setter
@Getter
@ToString
public class SearchVariantMatrixResponse extends SearchVariantMatrixBase {

    @ToString.Exclude
    String[][] data;

    List<GenotypeField> genotypeFields;

    List<DimensionPageResponse> pagination;

    List<String> variantSetDbIds;

    Set<Variant> variants;

    public SearchVariantMatrixResponse() {
        this.setExpandHomozygotes(true);
        this.setSepPhased(PHASED_SEP);
        this.setSepUnphased(UNPHASED_SEP);
        this.setUnknownString(UNKNOWN_STRING);
    }
}
