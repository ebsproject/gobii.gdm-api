package org.ebs.services.to;

import io.swagger.v3.oas.annotations.Parameter;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Getter
@Setter
@ToString
public class GetVariantMatrixParams {

    @Parameter(description = "The requested page size for the CallSet dimension of the matrix")
    String dimensionVariantPage;

    @Parameter(description = "The postion range to search. Not supported yet.")
    String positionRange;

    @Parameter(description = "Internal database identifier")
    String germplasmDbId;

    @Parameter(description = "Name of the germplasm")
    String germplasmName;

    @Parameter(description = "Permanent unique identifier (DOI, URI, etc.)")
    String germplasmPUI;

    @Parameter(description = "The ID of the CallSet to be retrieved.")
    String callSetDbId;

    @Parameter(description = "The ID of the Variant to be retrieved.")
    String variantDbId;

    @Parameter(description = "The ID of the VariantSet to be retrieved.")
    String variantSetDbId;

    @Parameter(description = "Should homozygotes be expanded (true) or collapsed into a single occurrence (false). Not supported yet, defaults to 'true'")
    boolean expandHomozygotes;

    @Parameter(description = "The string used as a separator for phased allele calls")
    String sepPhased = "|";

    @Parameter(description = "The string used as a separator for unphased allele calls")
    String sepUnphased = "/";

    @Parameter(description = "The string used as a representation for missing data")
    String unknownString = ".";

}
