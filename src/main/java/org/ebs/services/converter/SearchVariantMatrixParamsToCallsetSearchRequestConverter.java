package org.ebs.services.converter;

import static java.util.stream.Collectors.toSet;

import java.util.Collections;
import java.util.Optional;

import org.ebs.services.to.CallsetSearchRequest;
import org.ebs.services.to.SearchVariantMatrixParams;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class SearchVariantMatrixParamsToCallsetSearchRequestConverter
        implements Converter<SearchVariantMatrixParams, CallsetSearchRequest> {

    @Override
    public CallsetSearchRequest convert(SearchVariantMatrixParams source) {
        CallsetSearchRequest target = new CallsetSearchRequest();
        target.setCallSetDbIds(
                source.getCallSetDbIds().stream().map(Integer::valueOf).collect(toSet()));
        target.setVariantSetDbIds(source.getVariantSetDbIds());

        target.setGermplasmDbIds(Optional.ofNullable(source.getGermplasmDbIds())
                .map(g -> g.stream().map(Integer::valueOf).collect(toSet()))
                .orElse(Collections.emptySet()));
        target.setGermplasmNames(source.getGermplasmNames());
        target.setGermplasmPUIs(source.getGermplasmPUIs());

        target.setSampleDbIds(
                source.getSampleDbIds().stream().map(Integer::valueOf).collect(toSet()));
        return target;
    }

}
