package org.ebs.services.converter;

import org.ebs.model.MarkerModel;
import org.ebs.services.to.Variant;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
class MarkerModelToVariantConverter implements Converter<MarkerModel, Variant> {

    @Override
    public Variant convert(MarkerModel source) {
        Variant target = new Variant();

        target.setVariantDbId(source.getMarkerId().toString());
        target.setStart(0);
        target.setEnd(0);
        target.setPloidy(0);
        return target;
    }

}
