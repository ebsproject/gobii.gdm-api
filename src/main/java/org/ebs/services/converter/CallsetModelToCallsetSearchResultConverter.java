package org.ebs.services.converter;

import org.ebs.model.CallsetModel;
import org.ebs.services.to.CallsetSearchResult;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
public class CallsetModelToCallsetSearchResultConverter implements Converter<CallsetModel, CallsetSearchResult> {

    @Override
    public CallsetSearchResult convert(CallsetModel callset) {
        CallsetSearchResult result = new CallsetSearchResult();
        result.setSearchResultDbId(callset.getCallSetDbId().toString());
        return result;
    }

}
