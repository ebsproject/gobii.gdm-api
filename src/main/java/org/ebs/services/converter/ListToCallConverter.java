package org.ebs.services.converter;

import java.util.List;

import org.ebs.model.CallsetModel;
import org.ebs.model.MarkerModel;
import org.ebs.services.to.Call;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

//This converter may or may not be used once the results come from gdm extractors
@Deprecated
@Component
public class ListToCallConverter implements Converter<List<Object>, Call> {

    @Override
    public Call convert(List<Object> source) {
        Call target = new Call();
        CallsetModel callset = (CallsetModel) source.get(0);
        MarkerModel marker = (MarkerModel) source.get(2);

        target.setCallSetDbId(callset.getCallSetDbId());
        target.setCallSetName(callset.getCallSetName());
        target.setVariantDbId(marker.getMarkerId().toString());
        target.setVariantName(marker.getName());
        target.setAdditionalInfo(source.get(1).toString());

        return target;
    }

}
