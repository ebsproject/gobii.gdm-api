package org.ebs.services.converter;

import org.ebs.model.CallsetModel;
import org.ebs.model.DnaSampleModel;
import org.ebs.model.GermplasmModel;
import org.ebs.services.to.Callset;

import org.springframework.beans.BeanUtils;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;


@Component
class CallsetModelToCallsetConverter implements Converter<CallsetModel, Callset> {

    @Override
    public Callset convert(CallsetModel source) {
        Callset target = new Callset();
        BeanUtils.copyProperties(source, target);

        DnaSampleModel sample = source.getSample();
        GermplasmModel germplasm = sample.getGermplasm();

        target.setSampleName(sample.getDnaSampleName());
        target.setGermplasmDbId(germplasm.getGermplasmId());
        target.setGermplasmName(germplasm.getGermplasmName());
        target.setGermplasmType(germplasm.getGermplasmType());
        target.setAdditionalInfo(source.getProperties());

        return target;
    }

}
