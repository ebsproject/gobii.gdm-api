package org.ebs.services;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Optional;
import java.util.Set;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.ebs.model.CallsetModel;
import org.ebs.model.CvGroupTermEnum;
import org.ebs.model.CvModel;
import org.ebs.model.DnaSampleModel;
import org.ebs.model.GermplasmModel;
import org.ebs.model.repository.CallsetRepository;
import org.ebs.model.repository.CvRepository;
import org.ebs.services.to.Callset;
import org.ebs.services.to.CallsetSearchRequest;

import org.springframework.core.convert.ConversionService;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import lombok.RequiredArgsConstructor;


@Service
@Transactional(readOnly = true)
@RequiredArgsConstructor
class CallsetServiceImpl implements CallsetService {

    private final ConversionService converter;
    private final CallsetRepository callsetRepo;
    private final CvRepository cvRepo;

    @PersistenceContext
    private EntityManager entityManager;


    @Override
    public Optional<Callset> searchCallsets(Integer id) {
        HashMap<String, String> cvMap = getCvMap();
        return this.callsetRepo.findById(id).map(callset -> {
            callset.setProperties(mapCvProps(callset, cvMap));
            return converter.convert(callset, Callset.class);
        });
    }

    @Override
    public Page<Callset> searchCallsets(Pageable page) {
        return searchCallsets(new CallsetSearchRequest(), page);
    }

    @Override
    public Page<Callset> searchCallsets(CallsetSearchRequest params, Pageable page) {
        HashMap<String, String> cvMap = getCvMap();
        return this.callsetRepo.findAll((root, criteriaQuery, builder) -> {
            List<Predicate> predicates = assemblePredicates(builder, criteriaQuery, root, params);
            return builder.and(predicates.toArray(new Predicate[]{}));
        }, page).map(callset -> {
            callset.setProperties(mapCvProps(callset, cvMap));
            return converter.convert(callset, Callset.class);
        });
    }

    /**
     * Puts together the list of predicates for the query, based on the given Callset search parameters.
     * @param builder
     * @param qRoot
     * @param params
     * @return List of Predicates
     */
    private List<Predicate> assemblePredicates(CriteriaBuilder builder, CriteriaQuery<?> query, Root<CallsetModel> root, CallsetSearchRequest params) {
        List<Predicate> predicates = new ArrayList<>();
        HashMap<Set<?>, Predicate> paramValueToPredicate = new HashMap<>();

        Class<?> callsetModelClassOrCounting = query.getResultType();
        if (callsetModelClassOrCounting.equals(CallsetModel.class)) {
            @SuppressWarnings("unchecked")
            Join<Object, Object> dnaSampleJoin = (Join<Object, Object>) root.fetch("sample", JoinType.LEFT);
            @SuppressWarnings("unchecked")
            Join<Object, Object> germplasmJoin = (Join<Object, Object>) dnaSampleJoin.fetch("germplasm", JoinType.LEFT);

            // Map parameters with what predicate should be used if the parameter is not empty.
            paramValueToPredicate.put(params.getCallSetDbIds(), root.get("callSetDbId").in(params.getCallSetDbIds()));
            paramValueToPredicate.put(params.getCallSetNames(), root.get("callSetName").in(params.getCallSetNames()));
            paramValueToPredicate.put(params.getSampleDbIds(), root.get("sampleDbId").in(params.getSampleDbIds()));
            paramValueToPredicate.put(params.getSamplePUIs(), dnaSampleJoin.get("dnaSampleUuid").in(params.getSamplePUIs()));
            paramValueToPredicate.put(params.getSampleNames(), dnaSampleJoin.get("dnaSampleName").in(params.getSampleNames()));
            paramValueToPredicate.put(params.getGermplasmDbIds(), dnaSampleJoin.get("germplasmId").in(params.getGermplasmDbIds()));
            paramValueToPredicate.put(params.getGermplasmPUIs(), germplasmJoin.get("externalCode").in(params.getGermplasmPUIs()));
            paramValueToPredicate.put(params.getGermplasmNames(), germplasmJoin.get("germplasmName").in(params.getGermplasmNames()));
            paramValueToPredicate.put(
                params.getVariantSetDbIds(),
                builder.equal(
                    builder.function(
                        "JSONB_EXISTS_ANY",
                        Boolean.class,
                        root.get("datasetDnaRunIdx"),
                        builder.function(
                            "string_to_array",
                            String.class,
                            builder.literal(String.join(",", params.getVariantSetDbIds())),
                            builder.literal(",")
                        )
                    ),
                    true
                )
            );
        }

        paramValueToPredicate.forEach((paramValue, predicate) -> {
            if (!paramValue.isEmpty()) predicates.add(predicate);
        });

        return predicates;
    }

    /**
     * Get all possible CV terms for dnaRun, dnaSample, and germplasm from the database.
     * @return HashMap of {cv.id: cv.term}
     */
    private HashMap<String, String> getCvMap() {
        List<CvModel> relevantCvs = cvRepo.findByCvGroupCvGroupNameIn(Arrays.asList(
            CvGroupTermEnum.DNARUN_PROP.getTerm(),
            CvGroupTermEnum.DNASAMPLE_PROP.getTerm(),
            CvGroupTermEnum.GERMPLASM_PROP.getTerm()
        ));

        HashMap<String, String> cvMap = new HashMap<>();
        relevantCvs.forEach(cv -> cvMap.put(cv.getCvId().toString(), cv.getTerm()));

        return cvMap;
    }

    /**
     * Map the CV IDs from the callsets to their CV terms with values.
     * @param callset A single callset
     * @param cvMap The map of possible CVs from the database.
     * @return HashMap of {cv.term: value}
     */
    private HashMap<String, String> mapCvProps(CallsetModel callset, HashMap<String, String> cvMap) {
        DnaSampleModel sample = callset.getSample();
        GermplasmModel germplasm = sample.getGermplasm();

        HashMap<String, String> allCallsetProps = new HashMap<>();
        allCallsetProps.putAll(callset.getAdditionalInfo());
        allCallsetProps.putAll(sample.getProperties());
        allCallsetProps.putAll(germplasm.getProperties());

        HashMap<String, String> propsWithCvNames = new HashMap<>();
        allCallsetProps.forEach((cvId, value) -> {
            if (!value.isBlank()) {
                propsWithCvNames.put(cvMap.get(cvId), value);
            }
        });

        return propsWithCvNames;
    }

}
