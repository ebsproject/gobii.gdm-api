package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "dataset", schema = "public")
public class DatasetModel {

    @Id
    @Column(name = "dataset_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer datasetId;

    @Column
    private String name;
    @Column(name = "data_table")
    private String dataTable;
    @Column(name = "data_file")
    private String dataFile;
}
