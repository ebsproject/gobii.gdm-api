package org.ebs.model;

import lombok.AllArgsConstructor;
import lombok.Getter;


/**
 * To store CVGroup Terms used in the database.
 */
@AllArgsConstructor
public enum CvGroupTermEnum {
    ANALYSIS_TYPE("analysis_type"),
    DATASET_TYPE("dataset_type"),
    DNARUN_PROP("dnarun_prop"),
    DNASAMPLE_PROP("dnasample_prop"),
    GERMPLASM_PROP("germplasm_prop"),
    GERMPLASM_SPECIES("germplasm_species"),
    GERMPLASM_TYPE("germplasm_type"),
    GOBII_DATAWAREHOUSE("gobii_datawarehouse"),
    JOBSTATUS("job_status"),
    JOBTYPE("job_type"),
    MAPSET_TYPE("mapset_type"),
    MARKER_PROP("marker_prop"),
    MARKER_STRAND("marker_strand"),
    PAYLOADTYPE("payload_type"),
    PLATFORM_TYPE("platform_type"),
    PROJECT_PROP("project_prop"),
    STATUS("status");

    @Getter private String term;
}
