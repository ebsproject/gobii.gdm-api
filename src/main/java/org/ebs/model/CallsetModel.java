package org.ebs.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import javax.persistence.Transient;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;
import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

@Entity
@Table(name = "dnarun", schema = "public")
@Getter
@Setter
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
@ToString
public class CallsetModel {

    @Transient
    private List<String> datasetIds = new ArrayList<>();

    @Id
    @Column(name = "dnarun_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer callSetDbId;

    @Column(name = "name")
    private String callSetName;

    @Column(name = "experiment_id")
    private Integer studyDbId;

    @Column(name = "dnasample_id")
    private Integer sampleDbId;

    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(name = "dnasample_id", insertable = false, updatable = false)
    private DnaSampleModel sample;

    @Column(name = "dataset_dnarun_idx", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private JsonNode datasetDnaRunIdx;

    public List<String> getVariantSetDbIds() {
        datasetIds.clear();
        this.datasetDnaRunIdx.fieldNames().forEachRemaining(datasetIds::add);

        return datasetIds;
    }

    @Column(name = "props", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private Map<String, String> additionalInfo;

    @Transient
    private Map<String, String> properties;

}
