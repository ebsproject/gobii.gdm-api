package org.ebs.model;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "cvgroup")
@Getter
@Setter
@ToString
public class CvGroupModel {

    @Id
    @Column(name = "cvgroup_id")
    private Integer cvGroupId;

    @Column(name = "name")
    private String cvGroupName;

    @Column(name = "type")
    private Integer cvGroupType;

    @Column(name="props", columnDefinition = "jsonb")
    @Type(type="jsonb")
    private Map<String, Object> properties;

}
