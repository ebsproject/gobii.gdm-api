package org.ebs.model.repository;

import org.ebs.model.MarkerModel;
import org.springframework.data.jpa.repository.JpaRepository;

public interface MarkerRepository extends JpaRepository<MarkerModel, Integer>, MarkerRepositoryExt {

}
