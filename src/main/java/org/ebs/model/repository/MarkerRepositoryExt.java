package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.MarkerModel;

interface MarkerRepositoryExt {

    List<MarkerModel> findByDatasetId(int datasetId);

}
