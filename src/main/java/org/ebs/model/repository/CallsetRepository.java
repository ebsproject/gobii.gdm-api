package org.ebs.model.repository;

import java.util.List;

import org.ebs.model.CallsetModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.JpaSpecificationExecutor;

public interface CallsetRepository extends JpaRepository<CallsetModel, Integer>,
        RepositoryExt<CallsetModel>, JpaSpecificationExecutor<CallsetModel> {

    List<CallsetModel> findByCallSetDbIdIn(int[] callsetDbIds);
}
