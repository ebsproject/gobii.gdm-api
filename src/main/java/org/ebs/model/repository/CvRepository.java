package org.ebs.model.repository;

import java.util.Collection;
import java.util.List;
import org.ebs.model.CvModel;
import org.ebs.util.RepositoryExt;

import org.springframework.data.jpa.repository.JpaRepository;


public interface CvRepository extends JpaRepository<CvModel, Integer>, RepositoryExt<CvModel> {
    /**
     *
     * @param groupNames Names of the CV groups to return the CVs for
     * @return List of matching CV objects
     */
    List<CvModel> findByCvGroupCvGroupNameIn(Collection<String> groupNames);
}
