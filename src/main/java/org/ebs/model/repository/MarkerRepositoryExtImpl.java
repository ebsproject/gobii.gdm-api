package org.ebs.model.repository;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;

import org.ebs.model.MarkerModel;

class MarkerRepositoryExtImpl implements MarkerRepositoryExt {
    @PersistenceContext
    private EntityManager entityManager;

    @Override
    public List<MarkerModel> findByDatasetId(int datasetId) {
        CriteriaBuilder builder = entityManager.getCriteriaBuilder();
        CriteriaQuery<MarkerModel> query = builder.createQuery(MarkerModel.class);
        Root<MarkerModel> root = query.from(MarkerModel.class);

        return (List<MarkerModel>) entityManager
                .createQuery(query.select(root).orderBy(builder.asc(root.get("markerId")))
                        .where(datasetIdExists(builder, root, datasetId)))
                .getResultList();
    }

    private Predicate datasetIdExists(CriteriaBuilder builder, Root<MarkerModel> queryRoot,
            int datasetId) {

        return builder.isTrue(builder.function("JSONB_EXISTS", Boolean.class,
                queryRoot.get("datasetMarkerIdx"), builder.literal(String.valueOf(datasetId))));
    }
}