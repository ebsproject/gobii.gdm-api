package org.ebs.model.repository;

import org.ebs.model.DatasetModel;
import org.ebs.util.RepositoryExt;
import org.springframework.data.jpa.repository.JpaRepository;

public interface DatasetRepository
        extends JpaRepository<DatasetModel, Integer>, RepositoryExt<DatasetModel> {
}
