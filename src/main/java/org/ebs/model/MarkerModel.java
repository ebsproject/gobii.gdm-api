package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import com.fasterxml.jackson.databind.JsonNode;
import com.vladmihalcea.hibernate.type.json.JsonBinaryType;

import org.hibernate.annotations.Type;
import org.hibernate.annotations.TypeDef;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Entity
@Table(name = "marker", schema = "public")
@TypeDef(name = "jsonb", typeClass = JsonBinaryType.class)
public class MarkerModel {

    @Id
    @Column(name = "marker_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer markerId;

    @Column
    private String name;
    @Column
    private String code;

    @Column(name = "dataset_marker_idx", columnDefinition = "jsonb")
    @Type(type = "jsonb")
    private JsonNode datasetMarkerIdx;
}
