package org.ebs.model;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;


@Entity
@Table(name = "cv", schema = "public")
@Getter
@Setter
@ToString
public class CvModel {

    @Id
    @Column(name = "cv_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer cvId;

    @Column
    private String term;

    @Column
    private String definition;

    @Column
    private Integer rank;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "cvgroup_id")
    private CvGroupModel cvGroup;

    @Column
    private String abbreviation;

    @Column(name = "dbxref_id")
    private Integer dbxrefId;

    @Column
    private Integer status;

}
