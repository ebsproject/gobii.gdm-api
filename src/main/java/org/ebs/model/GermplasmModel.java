package org.ebs.model;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import lombok.Getter;
import lombok.Setter;
import org.hibernate.annotations.Type;


@Entity
@Table(name = "germplasm", schema = "public")
@Getter
@Setter
public class GermplasmModel {

    @Id
    @Column(name="germplasm_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Integer germplasmId;

    @Column(name="name")
    private String germplasmName;

    @Column(name="external_code")
    private String externalCode;

    @Column(name="species_id")
    private Integer germplasmSpecies;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "type_id", referencedColumnName = "cv_id")
    private CvModel germplasmTypeCv;

    public String getGermplasmType() {
        return germplasmTypeCv != null ? germplasmTypeCv.getTerm() : null;
    }

    @Column(name="code")
    private String code;

    @Column(name="props", columnDefinition = "jsonb")
    @Type(type="jsonb")
    private Map<String, String> properties;

}
