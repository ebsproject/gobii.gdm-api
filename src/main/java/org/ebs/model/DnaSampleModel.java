package org.ebs.model;

import java.util.Map;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

import org.hibernate.annotations.Type;
import lombok.Getter;
import lombok.Setter;


@Entity
@Table(name = "dnasample", schema = "public")
@Getter
@Setter
public class DnaSampleModel {

    @Id
    @Column(name="dnasample_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    // @OneToMany(mappedBy="sample")
    private Integer dnaSampleId;

    @Column(name="name")
    private String dnaSampleName;

    @Column(name="uuid")
    private String dnaSampleUuid;

    @Column(name="code")
    private Integer dnaSampleCode;

    @Column(name="platename")
    private String plateName;

    @Column(name="num")
    private String dnaSampleNum;

    @Column(name="well_row")
    private String wellRow;

    @Column(name="well_col")
    private String wellCol;

    // @ManyToOne(fetch = FetchType.LAZY)
    // @JoinColumn(name="project_id")
    // private Project project;

    @Column(name="germplasm_id")
    private Integer germplasmId;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "germplasm_id", insertable=false, updatable=false)
    private GermplasmModel germplasm;

    @Column(name="props", columnDefinition = "jsonb")
    @Type(type="jsonb")
    private Map<String, String> properties;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "status", referencedColumnName = "cv_id")
    private CvModel cv;

    public Integer getStatus() { return cv.getStatus(); }

}
