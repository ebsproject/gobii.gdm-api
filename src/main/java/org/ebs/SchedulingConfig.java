package org.ebs;

import org.ebs.services.FileResolver;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;

import lombok.RequiredArgsConstructor;

@Configuration
@EnableScheduling
@RequiredArgsConstructor
public class SchedulingConfig {

    @Value("${ebs.gobii.gdm.daysToRetain}")
    private int daysToRetain = 1;

    private final FileResolver fileResolver;

    /**
     * Runs twice a day the clean up routine for search result files
     */
    @Scheduled(cron = "0 0 0,12 * * ?")
    public void scheduledCleanUp() {
        fileResolver.cleanUpSearchResults(daysToRetain);
    }

}
