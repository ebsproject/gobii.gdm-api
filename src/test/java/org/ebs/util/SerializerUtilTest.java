package org.ebs.util;

import static org.junit.jupiter.api.Assertions.*;

import java.util.ArrayList;
import java.util.List;

import org.ebs.services.to.Call;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class SerializerUtilTest {

    @Test
    public void givenNotEmptyList_whenCallSubList_thenReturnSubList() {
        List<Call> fullList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            fullList.add(new Call());
            fullList.get(i).setCallSetDbId(i);
        }

        List<Call> page1 = SerializerUtil.callSubList(fullList, 0, 10);
        List<Call> page2 = SerializerUtil.callSubList(fullList, 1, 10);
        assertEquals(10, page1.size());
        assertEquals(5, page2.size());
    }

    @Test
    public void givenNotEmptyList_whenSubListChanges_thenOriginalListRemainsTheSame() {
        List<Call> fullList = new ArrayList<>();
        for (int i = 0; i < 15; i++) {
            fullList.add(new Call());
            fullList.get(i).setCallSetDbId(i);
        }
        List<Call> page1 = SerializerUtil.callSubList(fullList, 0, 10);
        List<Call> page2 = SerializerUtil.callSubList(fullList, 1, 10);

        page1.clear();
        page2.clear();
        assertEquals(15, fullList.size());
    }
}
