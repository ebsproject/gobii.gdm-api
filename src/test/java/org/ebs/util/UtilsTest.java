package org.ebs.util;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertEquals;

import org.junit.jupiter.api.Test;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.Setter;

public class UtilsTest {

    @Setter
    @Getter
    @AllArgsConstructor
    class UserModel extends Auditable {
        private int id;
        private String name;
        private Integer age;
        private PersonModel person;
    }

    @Setter
    @Getter
    @AllArgsConstructor
    class PersonModel extends Auditable {
        private int pid;
        private String pname;
    }

    @Setter
    @Getter
    @AllArgsConstructor
    class User {
        private int id;
        private String name;
        private Integer age;
        private Person person;
    }

    @Setter
    @Getter
    @AllArgsConstructor
    class Person {
        private int pid;
        private String pname;
    }

    @Test
    public void givenANumber_whenGetPaddingZeroes_thenReturnPaddedString() {

        assertEquals("000009", Utils.getPaddingZeroes(9, 6));
        assertEquals("000249", Utils.getPaddingZeroes(249, 6));
        assertEquals("0000012300", Utils.getPaddingZeroes(Integer.valueOf("12300"), 10));
        assertEquals("00456789", Utils.getPaddingZeroes(Integer.valueOf(456789), Integer.valueOf(8)));

    }

    @Test
    public void givenNullPropsInSource_whenCopyNotNulls_thenDoNotOverrideTargetWithNulls() {
        UserModel source = new UserModel(37, null, null, new PersonModel(123, "person name"));
        User target = new User(999, "old name", 24, null);

        Utils.copyNotNulls(source, target);

        assertThat(target).extracting("id", "name", "age", "person").containsExactly(37, "old name", 24, null);
    }

    @Test
    public void givenNotNullPropsInSource_whenCopyNotNulls_thenCopyEverything() {
        UserModel source = new UserModel(456, "new name", 45, new PersonModel(123, "person name"));
        User target = new User(999, "old name", 24, null);

        Utils.copyNotNulls(source, target);

        assertThat(target).extracting("id", "name", "age", "person").containsExactly(456, "new name", 45, null);
    }

    @Test
    public void givenNestedObjectsInSource_whenCopyNotNulls_thenNestOnDemand() {
        UserModel source = new UserModel(456, "new name", 45, new PersonModel(123, "person name"));
        User target = new User(999, "old name", 24, new Person(0, null));

        Utils.copyNotNulls(source, target);
        Utils.copyNotNulls(source.getPerson(), target.getPerson());

        assertThat(target).extracting("id", "name", "age", "person.pid", "person.pname").containsExactly(456,
                "new name", 45, 123, "person name");
    }
}
