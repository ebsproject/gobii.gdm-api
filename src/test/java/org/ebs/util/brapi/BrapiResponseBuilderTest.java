package org.ebs.util.brapi;

import static org.assertj.core.api.Assertions.assertThat;

import org.junit.jupiter.api.Test;

public class BrapiResponseBuilderTest {
    @Test
    public void givenTokenBasedPaginationRequired_whenForData_thenBuildTokenMetadata() {

        BrResponse<String> result = BrapiResponseBuilder.forData("Hello", "0", "1", "2").build();

        assertThat(result.getMetadata().getPagination())
                .extracting("prevPageToken", "currentPageToken", "nextPageToken")
                .contains("0", "1", "2");

    }
}
