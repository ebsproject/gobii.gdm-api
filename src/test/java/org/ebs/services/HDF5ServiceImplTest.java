package org.ebs.services;

import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.LinkedHashSet;
import java.util.List;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class HDF5ServiceImplTest {

    private HDF5ServiceImpl subject = null;

    @Mock
    private FileResolver fileResolverMock;

    @BeforeEach
    public void init() {
        subject = new HDF5ServiceImpl(fileResolverMock);
    }

    @Test
    public void givenSamples_whenSamplesForCut_thenReturnValidString() {
        String result = subject
                .samplesForCut(new LinkedHashSet<>(List.of(11, 22, -1, -1, 0, -1, 55, 44, -1, 33)));

        assertEquals("12,23,1,56,45,34", result);
    }

    @Test
    public void givenGenotypeResults_whenParseGenotype_thenUseUnphasedSep() {
        String genotype = String.join(HDF5Service.UNPHASED_SEP, "AG".split(""));
        assertEquals("A/G", genotype);
    }
}
