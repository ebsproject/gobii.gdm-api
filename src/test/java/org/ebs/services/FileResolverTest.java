package org.ebs.services;

import static java.util.Optional.of;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.Mockito.when;
import java.lang.reflect.Field;
import java.nio.file.Paths;

import org.ebs.model.DatasetModel;
import org.ebs.model.repository.DatasetRepository;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.Mock;
import org.mockito.junit.jupiter.MockitoExtension;


@ExtendWith(MockitoExtension.class)
public class FileResolverTest {

    private FileResolver subject = null;

    @Mock
    private DatasetRepository datasetRepositoryMock;

    @BeforeEach
    public void init() throws NoSuchFieldException, SecurityException, IllegalAccessException {
        subject = new FileResolver(datasetRepositoryMock);
        Field field = subject.getClass().getDeclaredField("dataPath");
        field.setAccessible(true);
        field.set(subject, "/data-test");
    }

    @Test
    public void givenDatasetIncludingGlobalPath_whenResolveForHDF5_thenReturnTrimmedPath() {
        DatasetModel object = new DatasetModel();
        object.setDataFile("/data-test/some/other/path.h5");

        String result = subject.resolveForHDF5("maize", 123).toString();
        String expected = Paths.get("/data-test", "hdf5", "DS_123.h5").toString();

        assertEquals(expected, result);

    }

    @Test
    public void givenDatasetNotIncludingGlobalPath_whenResolveForHDF5_thenReturnUnmodifiedPath() {
        DatasetModel object = new DatasetModel();
        object.setDataFile("/some/basic/path.h5");

        String result = subject.resolveForHDF5("maize", 123).toString();
        String expected = Paths.get("/data-test", "hdf5", "DS_123.h5").toString();

        assertEquals(expected, result);
    }

}
