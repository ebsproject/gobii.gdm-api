package org.ebs.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


public class CallsetModelTest {

    private CallsetModel subject;

    @BeforeEach
    public void init() {
        subject = new CallsetModel();
        DnaSampleModel sample = new DnaSampleModel();
        sample.setDnaSampleName("sample1");
        subject.setSample(sample);
    }

    @Test
    public void getSampleName() {
        assertEquals("sample1", subject.getSample().getDnaSampleName());
    }

}
