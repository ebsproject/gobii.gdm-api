package org.ebs.model;

import static org.junit.jupiter.api.Assertions.assertEquals;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;


class DnaSampleModelTest {

    private DnaSampleModel subject;

    @BeforeEach
    public void init() {
        subject = new DnaSampleModel();
        subject.setDnaSampleName("sample1");
    }

    @Test
    void getSampleName() {
        assertEquals("sample1", subject.getDnaSampleName());
    }

}
